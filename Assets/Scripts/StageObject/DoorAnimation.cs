﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

public class DoorAnimation : MonoBehaviour
{
    public enum AnimationName
    {
        LeftDoor_Open,
        LeftDoor_Close,
        RightDoor_Open,
        RightDoor_Close,
    }
    [SerializeField] AudioSource audioSource;
    [SerializeField] GameObject leftDoor;
    [SerializeField] GameObject rightDoor;
    [SerializeField] List<DoorVibrate> vibrations;
    [SerializeField] Animator leftDoor_Animator;
    [SerializeField] Animator rightDoor_Animator;
    [SerializeField, Header("アニメーション再生速度")] float animationSpeed = 0.12f;
    private void Awake()
    {
        leftDoor_Animator.enabled = false;
        rightDoor_Animator.enabled = false;
        leftDoor_Animator.speed = animationSpeed;
        rightDoor_Animator.speed = animationSpeed;
    }

    /// <summary>扉を開く</summary>
    public void OpenDoor()
    {
        foreach (var vibration in vibrations)
        {
            vibration.Vibrate();
        }

        audioSource.Play();
        leftDoor_Animator.enabled = true;
        rightDoor_Animator.enabled = true;
        leftDoor_Animator.CrossFadeInFixedTime(AnimationName.LeftDoor_Open.ToString(), 0);
        rightDoor_Animator.CrossFadeInFixedTime(AnimationName.RightDoor_Open.ToString(), 0);
    }

    /// <summary>扉を閉じる</summary>
    public void CloseDoor()
    {
        audioSource.Play();
        leftDoor_Animator.CrossFadeInFixedTime(AnimationName.LeftDoor_Close.ToString(), 0);
        rightDoor_Animator.CrossFadeInFixedTime(AnimationName.RightDoor_Close.ToString(), 0);
    }

    private void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.O))
        {
            OpenDoor();
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            CloseDoor();
        }
#endif
    }
}
