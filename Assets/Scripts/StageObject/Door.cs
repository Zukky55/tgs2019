﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
public class Door : MonoBehaviour
{
    public bool IsCharged => actualValue == 1f;
    public Transform TargetNode => targetNode;

    /// <summary>実際にemissionに代入している値</summary>
    float actualValue;
    [HideInInspector] public FloatReactiveProperty EmissionAmount;
    [SerializeField] float fadingTime = 20f;
    [SerializeField] Transform targetNode;
    Material mat;

    /// <summary>Value animationを続ける判定の閾値</summary>
    const float threshold = 0.01f;


    private void Awake()
    {
        mat = GetComponent<Renderer>().material;
    }
    private void Start()
    {
        this.OnTriggerEnterAsObservable()
            .Where(col => col.gameObject.tag == ObjTag.TutorialObj.ToString())
            .Subscribe(_ =>
            {
                //TODO: 魔法が扉に当たった時のイベント
            })
            .AddTo(this);

        /// <see cref="EmissionAmount"/>に値が来たら,0~1でClampして,その値に<see cref="fadingTime"/>秒でlerpさせる
        EmissionAmount
            .Select(amount =>
            {
                if (amount > 1f)
                    amount = 1f;
                if (amount < 0f)
                    amount = 0f;
                return amount;
            })
            .Subscribe(amount =>
            {
                SyncDoorEmission(amount);
            })
            .AddTo(this);
    }

    void SyncDoorEmission(float amount)
    {
        float startTime, elapsedTime, rate;
        actualValue = elapsedTime = rate = 0f;
        startTime = Time.timeSinceLevelLoad;
        var flag = true;

        this.UpdateAsObservable()
        .TakeWhile(_ => flag)
      .Subscribe(_ =>
      {
          elapsedTime = Time.timeSinceLevelLoad - startTime;
          if (elapsedTime > fadingTime || Mathf.Abs(amount - actualValue) < threshold)
          {
              actualValue = amount;
              flag = false;
          }
          rate = elapsedTime / fadingTime;
          actualValue = Mathf.Lerp(actualValue, amount, rate);
          mat.SetFloat("_emissionAmount", actualValue);
          //Debug.Log($"rate= {rate}: actualValue = {actualValue}: elapsedTime = {elapsedTime}: fadingTime : {fadingTime}");
      })
      .AddTo(this);
    }

    public void ActivateDoor()
    {
        EmissionAmount.Value = 1f;
    }
}
