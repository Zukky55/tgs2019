﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BillboardObject
{
    /// <summary>
    /// 指定したオブジェクトを指定したオブジェクトの方向に向かせる
    /// </summary>
    /// <param name="obj">向かせたいオブジェクト</param>
    /// <param name="targetObj">ターゲット</param>
    public static void Billboard(ref GameObject obj,GameObject targetObj)
    {
        var target = targetObj.transform.position;
        obj.transform.rotation = Quaternion.LookRotation(obj.transform.position - target);
    }
}
