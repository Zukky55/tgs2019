﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

public class DoorVibrate : MonoBehaviour
{
    [Range(0, 1)] [SerializeField] private float vibrateRange; //振動幅
    [SerializeField] private float vibrateSpeed;               //振動速度
    private Animator a;
    private float initPosition;   //初期ポジション
    private float newPosition;    //新規ポジション
    private float minPosition;    //ポジションの下限
    private float maxPosition;    //ポジションの上限
    private bool directionToggle;
    public bool vibrationFlag ;

    IDisposable vibrateDisposable;
    public void Vibrate()
    {
        a = this.gameObject.GetComponentInChildren<Animator>();
        this.initPosition = transform.localPosition.y;
        this.newPosition = this.initPosition;
        this.minPosition = this.initPosition - this.vibrateRange;
        this.maxPosition = this.initPosition + this.vibrateRange;
        this.directionToggle = false;

        vibrateDisposable = Observable.EveryFixedUpdate()
        .Subscribe(_ =>
        {
            //ポジションが振動幅の範囲を超えた場合、振動方向を切り替える
            if (this.newPosition <= this.minPosition ||
                this.maxPosition <= this.newPosition)
            {
                this.directionToggle = !this.directionToggle;
            }

            //新規ポジションを設定
            this.newPosition = this.directionToggle ?
                this.newPosition + (vibrateSpeed * Time.deltaTime) :
                this.newPosition - (vibrateSpeed * Time.deltaTime);
            this.newPosition = Mathf.Clamp(this.newPosition, this.minPosition, this.maxPosition);
            this.transform.localPosition = new Vector3(transform.localPosition.x + newPosition
                , transform.localPosition.y + newPosition
                , transform.localPosition.z );

            //if (a.GetCurrentAnimatorStateInfo(0).normalizedTime == 1)
            //{
            //    disposable.Dispose();
            //}
        });

    }
    public void OnCompleted()
    {
        vibrateDisposable.Dispose();
    }
}
