﻿using System;

[Flags]
public enum ObjTag
{
    Player,
    Grabbable,
    FieldMap,
    Enemy,
    MagicBullet,
    Sword,
    MagicHand,
    RedHand,
    BlueHand,
    Door,
    TutorialObj,
    RestartButton,
}

public enum EnemyTag
{
    /// <summary>歩いてくる</summary>
    Walker,
    /// <summary>マジシャン</summary>
    Magician,
    /// <summary>ウェーブボス</summary>
    WaveBoss,
}