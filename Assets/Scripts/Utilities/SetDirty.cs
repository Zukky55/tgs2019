﻿#if UNITY_EDITOR
using UnityEditor;

public static class SetDirty
{
    [MenuItem("Assets/SetDirty")]
    static void Set()
    {
        foreach (var obj in Selection.objects)
        {
            EditorUtility.SetDirty(obj);
        }
        AssetDatabase.SaveAssets();
    }
}
#endif