﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

public class Conductor : MonoSingleton<Conductor>
{
    /// <summary></summary>
    public float SecondPerBeat { get; private set; }
    /// <summary>到達する迄の目標時間</summary>
    public float TargetTime => SecondPerBeat * multiplyByBeat;
    /// <summary>発射から何ビート目に到達するかどうかの係数</summary>
    public int MultiplyByBeat { get => multiplyByBeat; set => multiplyByBeat = value; }
    /// <summary></summary>
    public Player Player => player ? player : GameObject.FindGameObjectWithTag(ObjTag.Player.ToString()).GetComponent<Player>();
    /// <summary></summary>
    public List<Enemy> Enemies { get => enemies; set => enemies = value; }
    public List<MagicBullet> MagicBullets { get; private set; } = new List<MagicBullet>();

    [SerializeField] int multiplyByBeat = 2;

    List<Enemy> enemies = new List<Enemy>();
    Player player;

    public void Start()
    {
        SecondPerBeat = 0.5454f;
        #region Set second per beat.
        this.UpdateAsObservable()
            .Where(_ => Music.IsJustChangedBeat())
            .Timestamp()
            .Pairwise((x, y) => y.Timestamp - x.Timestamp)
            .Subscribe(ts =>
            {
                SecondPerBeat = (float)ts.TotalSeconds;
            })
            .AddTo(this);
        #endregion
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="enemy"></param>
    public void SubscribeEnemy(Enemy enemy) => Enemies?.Add(enemy);
    /// <summary>
    /// 
    /// </summary>
    /// <param name="enemy"></param>
    internal void UnSubscribeEnemy(Enemy enemy) => Enemies?.Remove(enemy);
    /// <summary>
    /// 
    /// </summary>
    /// <param name="enemy"></param>
    public void SubscribeMagicBullet(MagicBullet magicBullet) => MagicBullets?.Add(magicBullet);
    /// <summary>
    /// 
    /// </summary>
    /// <param name="enemy"></param>
    internal void UnSubscribeMagicBullet(MagicBullet magicBullet) => MagicBullets?.Remove(magicBullet);
}
