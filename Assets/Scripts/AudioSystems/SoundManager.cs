﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using UniRx;
using UniRx.Triggers;

public class SoundManager : MonoSingleton<SoundManager>
{
    public enum BGM
    {
        Stage,
        Result,
    }

    public enum SE
    {
        Scratch,
    }

    [SerializeField] AudioSource bgmAudioSource;
    [SerializeField] AudioSource seAudioSource;

    [SerializeField] BGMDataList bgmDataList;
    [SerializeField] SEDataList seDataList;
    /// <summary>曲が再生中か</summary>
    public bool isPlaying = false;
    GameManager gameManager;
    private void Start()
    {
        gameManager = GameManager.Instance;
        bgmAudioSource.playOnAwake = false;
        seAudioSource.playOnAwake = false;
 
        this.UpdateAsObservable()
           .Where(_ => isPlaying && !bgmAudioSource.isPlaying)//再生終了したら
           .Subscribe(_ =>
           {
               //クリアイベントを呼ぶ
               switch(ObservableGameEventTrigger.CurrentEvent)
               {
                   case ObservableGameEventTrigger.GameEvent.Stage:
                       ObservableGameEventTrigger.SetGameEvent(ObservableGameEventTrigger.GameEvent.StageClearEvent);
                       break;
               }
           }).AddTo(this);
    }

    /// <summary>
    /// BGMを再生する
    /// </summary>
    /// <param name="bgm"></param>
    public void PlayBGM(BGM bgm,bool isLoop)
    {
        BGMData bgmData = null;

        switch(bgm)
        {
            case BGM.Stage:
                bgmData = bgmDataList.BGMDatas.FirstOrDefault(bg => bg.BgmID == BGM.Stage);
                break;
            case BGM.Result:
                bgmData = bgmDataList.BGMDatas.FirstOrDefault(bg => bg.BgmID == BGM.Result);
                break;
            default:
                break;
        }

        if(isLoop)
            bgmAudioSource.loop = true;
        else
            bgmAudioSource.loop = false;

        bgmAudioSource.clip = bgmData.AudioClip;
        bgmAudioSource.Play();
        isPlaying = true;
        gameManager.StartMusicTime();
        Debug.Log($"{bgmData.BgmID}のBGMを再生");
    }

    /// <summary>
    /// BGMを停止する
    /// </summary>
    public void StopBGM()
    {
        bgmAudioSource.Stop();
        isPlaying = false;
    }

    ///// <summary>
    ///// SEを再生する
    ///// </summary>
    ///// <param name="se"></param>
    //public void PlaySE(SE se)
    //{
    //    SEData seData = null;
    //    switch(se)
    //    {
    //        case SE.Scratch:
    //            seData = seDataList.SEDatas.FirstOrDefault(s => s.SeID == SE.Scratch);
    //            break;
    //        default:
    //            break;
    //    }
    //    seAudioSource.PlayOneShot(seData.AudioClip);
    //}
}