﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System;

/// <summary>
/// 譜面の識別子
/// </summary>
public enum MusicScoreID
{
    Stage,
}


/// <summary>
/// CSV形式の譜面データをロードする
/// </summary>
public class MusicScoreLoader : MonoSingleton<MusicScoreLoader>
{
    [SerializeField, Header("ステージの譜面")] List<MusicScoreData> musicScoreList;
    int arraySize = 300;
    //[HideInInspector] public float[] timings;
    //[HideInInspector] public int[] enemyNode;
    //[HideInInspector] public int[] bulletTypes;
    [HideInInspector] public List<float> timings;
    [HideInInspector] public List<int> enemyNode;
    [HideInInspector] public List<int> bulletTypes;


    private void Start()
    {
        InitializeArray();
    }

    /// <summary>
    /// 譜面のデータ配列を初期化する
    /// </summary>
    public void InitializeArray()
    {
        //timings = new float[arraySize];
        //enemyNode = new int[arraySize];
        //bulletTypes = new int[arraySize];
        timings.Clear();
        enemyNode.Clear();
        bulletTypes.Clear();
    }

    /// <summary>譜面からデータを読み取る</summary>
    public void LoadMusicScore(MusicScoreID musicScoreID)
    {
        Debug.Log($"{musicScoreID}の譜面をロード");
        MusicScoreData musicScoreData = musicScoreList.Find(musicScore => musicScore.MusicScoreID == musicScoreID);

        StringReader reader = new StringReader(musicScoreData.MusicScore.text);
        // int i = 0;
        while(reader.Peek() > -1)
        {
            string line = reader.ReadLine();
            string[] values = line.Split(',');
            //for(int j = 0; j < values.Length; j++)
            //{
            //timings[i] = float.Parse(values[0]);
            //enemyNode[i] = int.Parse(values[1]);
            //bulletTypes[i] = int.Parse(values[2]);
            timings.Add(float.Parse(values[0]));
            enemyNode.Add(int.Parse(values[1]));
            bulletTypes.Add(int.Parse(values[2]));

            //}
            //i++;
        }

        ScoreManager scoreManager = ScoreManager.Instance;
        scoreManager.AllNotes = timings.Count;
        scoreManager.CalculationFullScore();

        foreach(var bulletType in bulletTypes)
        {
            if(GetMagicBulletSpecifier(bulletType) == MagicBulletSpecifier.RedMagic || GetMagicBulletSpecifier(bulletType) == MagicBulletSpecifier.BlueMagic)
            {
                scoreManager.BounceNotes++;
            }
        }
        Debug.Log($"全ノーツ={scoreManager.AllNotes},弾けるノーツ={scoreManager.BounceNotes}");
    }

    /// <summary>
    /// 譜面から魔法の種類を読み取る
    /// </summary>
    /// <returns></returns>
    MagicBulletSpecifier GetMagicBulletSpecifier(int bulletType)
    {
        MagicBulletSpecifier bulletSpecifier = MagicBulletSpecifier.RedMagic;
        MagicBulletSpecifier temp = (MagicBulletSpecifier)bulletType;
        switch(temp)
        {
            case MagicBulletSpecifier.RedMagic:
                bulletSpecifier = MagicBulletSpecifier.RedMagic;
                break;
            case MagicBulletSpecifier.BlueMagic:
                bulletSpecifier = MagicBulletSpecifier.BlueMagic;
                break;
            case MagicBulletSpecifier.NotBounceBackMagic:
                bulletSpecifier = MagicBulletSpecifier.NotBounceBackMagic;
                break;
            case MagicBulletSpecifier.BothMagic:
                bulletSpecifier = MagicBulletSpecifier.BothMagic;
                break;
            default:
                break;
        }

        return bulletSpecifier;
    }
}
