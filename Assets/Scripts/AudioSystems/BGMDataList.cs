﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable, CreateAssetMenu(fileName = "SoundData/BGMDataList")]
public class BGMDataList : ScriptableObject
{
    [SerializeField] List<BGMData> bgmDatas;
    public List<BGMData> BGMDatas { get { return bgmDatas; } }
}


[Serializable]
public class BGMData 
{
    [SerializeField] SoundManager.BGM bgm;
    [SerializeField] AudioClip audioClip;

    public SoundManager.BGM BgmID { get { return bgm; } }
    public AudioClip AudioClip { get { return audioClip; } }
}
