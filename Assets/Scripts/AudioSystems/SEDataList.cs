﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable, CreateAssetMenu(fileName = "SoundData/SEDataList")]
public class SEDataList : ScriptableObject
{
    [SerializeField] List<SEData> seDatas;
    public List<SEData> SEDatas { get { return seDatas; } }
}


[Serializable]
public class SEData
{
    [SerializeField] SoundManager.SE se;
    [SerializeField] AudioClip audioClip;

    public SoundManager.SE SeID { get { return se; } }
    public AudioClip AudioClip { get { return audioClip; } }
}
