﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UniRx;
using UniRx.Triggers;

/// <summary>
/// Base class of Enemy objects.
/// </summary>
public class TargetEnemy : Enemy
{
    [SerializeField, Header("出現するステージ")] ObservableGameEventTrigger.GameEvent gameEvent;
}
