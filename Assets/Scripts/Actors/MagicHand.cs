﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

/// <summary>
/// 
/// </summary>
public class MagicHand : MonoBehaviour
{
    public MagicBulletSpecifier Mbs => mbs;
    public Vector3 Velocity => velocity;

    [SerializeField] MagicBulletSpecifier mbs;
    OVRGrabber oVRGrabber;
    Vector3 velocity;

    private void Awake()
    {
        oVRGrabber = GetComponent<OVRGrabber>();
    }

    private void Start()
    {
        // 自分のvelocityを算出して公開
        this.UpdateAsObservable()
            .Select(_ => transform.position)
            .Pairwise((previousPos, currentPos) => currentPos - previousPos)
            .Subscribe(diff => velocity = diff)
            .AddTo(this);

        //弾に当たった時Vibrationを作動する
        this.OnTriggerEnterAsObservable()
       .Where(col => col.gameObject.tag == ObjTag.MagicBullet.ToString())
       .Select(col => col.GetComponent<MagicBullet>())
       .Where(mb => mb.Identity == mbs)
       .Subscribe(mb =>
       {
           ActivateVibration();
       })
       .AddTo(this);
    }
    public void ActivateVibration()
    {
        OVRInput.SetControllerVibration(.1f, .5f, oVRGrabber.Controller);
        Observable.Timer(TimeSpan.FromSeconds(.25))
        .Subscribe(_ =>
        {
            OVRInput.SetControllerVibration(0, 0, oVRGrabber.Controller);
        })
           .AddTo(this);
    }
}
