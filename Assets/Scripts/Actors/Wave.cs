﻿using UnityEngine;
using UniRx;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UniRx.Triggers;

[Serializable]
public class Wave : MonoBehaviour
{
    /// <summary>Waveが終わったかどうか </summary>
    public bool IsWavesEnd { get; private set; }

    /// <summary>waveに出現させる敵郡</summary>
    [SerializeField] List<EnemyTag> enemiesKind;


    void SpawnEnemy(EnemyTag tag)
    {
        GameObject enemy;
        switch (tag)
        {
            case EnemyTag.Walker:
                //enemy = Instantiate()
                break;
            case EnemyTag.WaveBoss:
                break;
            default:
                break;
        }
    }
}
