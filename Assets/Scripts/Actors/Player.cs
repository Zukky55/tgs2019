﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using UniRx;
using UniRx.Triggers;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public Transform CenterTrackingAnchor => centerTrackingAnchor;
    public MagicHand BlueHand => blueHand;
    public MagicHand RedHand => redHand;

    [SerializeField] Transform centerTrackingAnchor;
    [SerializeField] MagicHand blueHand;
    [SerializeField] MagicHand redHand;
    //Subject<int> sub = new Subject<int>();
    Texture2D screenTexture;
    private bool GuiCheck = false;
    Color DamegeColor;
    private void Awake()
    {
        screenTexture = new Texture2D(1, 1, TextureFormat.ARGB32, false);
        DamegeColor = new Color(255, 0, 0, 0.3f);
        screenTexture.SetPixel(0, 0, DamegeColor);
        screenTexture.Apply();
    }
    private void Start()
    {
        this.OnTriggerStayAsObservable()
           .Where(col => col.gameObject.layer == LayerMask.NameToLayer(ObjTag.MagicBullet.ToString()))
           .Subscribe(col =>
           {
               GuiCheck = true;
           })
        .AddTo(this);
    }

    /// <summary>
    ///  Taking damage on player, Then playing effect. 
    /// </summary>
    public void TakeDamage()
    {
        GuiCheck = true;
    }

    public void OnGUI()
    {
        if (GuiCheck)
        {
            GUI.DrawTexture(Camera.main.pixelRect, screenTexture);
            Observable.Timer(TimeSpan.FromSeconds(1f))
            .Subscribe(_ => GuiCheck = false)
            .AddTo(this);
        }
    }
}
