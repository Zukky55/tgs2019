﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

public class Audience : MonoBehaviour
{
    public EnemySpecifier Identity => identity;

    [SerializeField] GameObject spawnEffect;
    [SerializeField] EnemySpecifier identity;

    Conductor conductor;

    private void OnEnable()
    {
        conductor = Conductor.Instance;
        var player = conductor.Player.transform;
        transform.rotation = Quaternion.LookRotation(player.position - transform.position, Vector3.up);
        var rot = Quaternion.AngleAxis(-90, Vector3.right);
        Destroy(Instantiate(spawnEffect, transform.position, rot ), 5f);

    }
}
