﻿using UnityEngine;
using System;
using System.Collections.Generic;

/// <summary>
/// 敵のステータス
/// </summary>
[Serializable]
[CreateAssetMenu(fileName = "Status",menuName = "Enemy/Status")]
public class EnemyStatus : ScriptableObject
{
    /// <summary>自分自身の識別タグ</summary>
    public EnemySpecifier EnemySpecifier { get { return enemySpecifier; } }
    /// <summary>自分自身の識別タグ</summary>
    public Role EnemyRole { get { return enemyRole; } }
    /// <summary>体力</summary>
    public int HP { get => hp; set => hp = value; }
    public GameObject RedMagicBullet { get { return bounceBackMagicBullet_LeftHand; } }
    public GameObject BlueMagicBullet { get { return bounceBackMagicBullet_RightHand; } }
    public GameObject BothMagicBullet { get { return bounceBackMagicBullet_BothHand; } }
    public GameObject NotBounceBackMagicBullet { get { return notBounceBackMagicBullet; } }
    public GameObject RedFireEffect { get { return redFireEffect; } }
    public GameObject BlueFireEffect { get { return blueFireEffect; } }

    [SerializeField] EnemySpecifier enemySpecifier;
    [SerializeField] Role enemyRole;
    [SerializeField] int hp;
    [SerializeField] GameObject bounceBackMagicBullet_LeftHand;
    [SerializeField] GameObject bounceBackMagicBullet_RightHand;
    [SerializeField] GameObject bounceBackMagicBullet_BothHand;
    [SerializeField] GameObject notBounceBackMagicBullet;
    [SerializeField] GameObject redFireEffect;
    [SerializeField] GameObject blueFireEffect;

    public void Initialize(EnemyStatus enemyStatus)
    {
        enemySpecifier = enemyStatus.enemySpecifier;
        hp = enemyStatus.hp;
        bounceBackMagicBullet_LeftHand = enemyStatus.bounceBackMagicBullet_LeftHand;
        bounceBackMagicBullet_RightHand = enemyStatus.bounceBackMagicBullet_RightHand;
        notBounceBackMagicBullet = enemyStatus.notBounceBackMagicBullet;
        redFireEffect = enemyStatus.redFireEffect;
        blueFireEffect = enemyStatus.blueFireEffect;
    }
    /// <summary>
    /// 敵の役割
    /// </summary>
    public enum Role
    {
        RedMagic,
        BlueMagic,
        DisturbingMagic,
        Target,
    }
}