﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UniRx;
using UniRx.Triggers;

/// <summary>
/// Base class of Enemy objects.
/// </summary>
public class Enemy : MonoBehaviour
{
    [HideInInspector] public bool animTrigger;
    public SpawnNode enemySpawnNode;

    [SerializeField] protected EnemyStatus masterData;
    [SerializeField] protected GameObject redSpawnEffect;
    [SerializeField] protected GameObject blueSpawnEffect;

    private Animator anim;
    protected Player player;
    protected EnemyStatus status;
    protected int hp;
    Conductor conductor;
    protected virtual void Awake()
    {
        anim = GetComponent<Animator>();
        status = ScriptableObject.CreateInstance<EnemyStatus>();
        status.Initialize(masterData);
        conductor = Conductor.Instance;
        conductor?.SubscribeEnemy(this);
        player = conductor.Player;
        hp = status.HP;
    }
    protected virtual void Start()
    {
        // 敵をプレイヤーの方向,且つ水平方向に向かせる
        transform.rotation = Quaternion.LookRotation(player.transform.position - transform.position, Vector3.up);
    }
    /// <summary>
    /// Destory myself.
    /// </summary>
    public virtual void DestoryMyself()
    {
        // TODO: Implement a death event.
        conductor?.UnSubscribeEnemy(this);
        Destroy(gameObject);
    }

    public void SetSpawnNode(SpawnNode spawnNode)
    {
        enemySpawnNode = spawnNode;
    }

    /// <summary>
    /// 魔法弾を生成する
    /// </summary>
    public virtual void ShotMagicBullet(MagicBulletSpecifier magicBulletType,float targetTiming)
    {
        GameObject bullet = null;
        switch (magicBulletType)
        {
            case MagicBulletSpecifier.RedMagic:
                Destroy(Instantiate(status.RedFireEffect, transform.position + transform.forward, Quaternion.identity), 10f);
                break;
            case MagicBulletSpecifier.BlueMagic:
                Destroy(Instantiate(status.BlueFireEffect, transform.position + transform.forward, Quaternion.identity), 10f);
                break;
            default:
                break;
        }

        // ノーツの射出アニメーションが終わったらノーツを射出する
        switch (magicBulletType)
        {
            case MagicBulletSpecifier.RedMagic:
                bullet = Instantiate(status.RedMagicBullet, transform.position + transform.forward, Quaternion.identity);
                break;
            case MagicBulletSpecifier.BlueMagic:
                bullet = Instantiate(status.BlueMagicBullet, transform.position + transform.forward, Quaternion.identity);
                break;
            case MagicBulletSpecifier.BothMagic:
                //    bullet = Instantiate(status.BothHands_BounceBackMagicBullet,transform.position + transform.forward,Quaternion.identity);
                break;
            case MagicBulletSpecifier.NotBounceBackMagic:
                //bullet = Instantiate(status.NotBounceBackMagicBullet,transform.position + transform.forward,Quaternion.identity);
                break;
            default:
                break;
        }
        if (bullet == null)
            return;

        // 玉の初期化
        var mb = bullet.GetComponent<MagicBullet>();
        mb.InitMB(targetTiming);
        animTrigger = false;

        GameManager.Instance.shotCount++;
        //Debug.Log(GameManager.Instance.shotCount);
    }

    public enum AnimParameter
    {
        /// <summary></summary>
        attackTrigger,

    }
    enum AnimState
    {
        /// <summary></summary>
        Idle,
        /// <summary></summary>
        Attack,
    }
}