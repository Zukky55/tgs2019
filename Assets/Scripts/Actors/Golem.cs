﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Linq;
using UniRx;
using UniRx.Triggers;

public class Golem : MonoBehaviour
{
    public List<Transform> TargetNodes => targetNodes;

    /// <summary>Danceするのに必要なエネルギー</summary>
    [SerializeField] FloatReactiveProperty energy = new FloatReactiveProperty(0f);

    [SerializeField] Animator anim;
    [SerializeField] Material mat;
    [SerializeField] float downSpeed;
    [SerializeField] List<Transform> targetNodes;

    private void Start()
    {
        var update = this.UpdateAsObservable();

        update
            .Where(_ => energy.Value > 0)
            .Subscribe(_ =>
            {
                energy.Value -= Time.deltaTime * downSpeed;
                if (energy.Value < 0) energy.Value = 0f;
            })
            .AddTo(this);

        // EnergyとemissionAmountの同期
        energy
            .Where(e => e <= 1)
            .Subscribe(energy =>
            {
                anim.SetFloat("Energy", energy);
                mat.SetFloat("_emissionAmount", energy);
            })
            .AddTo(this);

        this.OnGameEventAsObservable()
            .Where(ge => ge == ObservableGameEventTrigger.GameEvent.Result)
            .Subscribe(ge =>
            {
                anim.SetTrigger("Result");
            })
            .AddTo(this);
    }

    /// <summary>
    /// set the energy.
    /// argument range is 0~1.
    /// </summary>
    /// <param name="addEnergy"></param>
    public void SetEnergy(float addEnergy)
    {
        if (addEnergy > 1f) addEnergy = 1f;
        if (addEnergy > 0f)
        {
            energy.Value += addEnergy;
        }
    }
}

/// <summary>
/// Golemのアニメーションパラメーター
/// </summary>
public enum AnimParameter
{
    LeftArmTrigger,
    RightArmTrigger,
    BodyTrigger,
    Miss,
}

public enum GolemTag
{
    GolemRightArm,
    GolemLeftArm,
    GolemBody,
}