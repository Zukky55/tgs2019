﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using Random = UnityEngine.Random;

/// <summary>
/// The magic object.
/// </summary>
public class MagicBullet : MonoBehaviour
{
    static Golem golem;

    /// <summary＞<see cref="identity"/></summary>
    public MagicBulletSpecifier Identity => identity;
    public Vector3 Velocity { get => velocity; set => velocity = value; }

    /// <summary>魔法のパラメータ</summary>
    [SerializeField] MagicObjParameter param;
    /// <summary>自分自身の魔法の種類</summary>
    [SerializeField] MagicBulletSpecifier identity;
    /// <summary>Object of  explosion effect</summary>
    [SerializeField] GameObject explosionObj;
    /// <summary><see cref="notesUI"/>を出す座標</summary>
    [SerializeField] Transform notesStatusUISpawnNode;
    /// <summary>NotesUI. include component of NotesStatusReseiver.</summary>
    [SerializeField] GameObject notesUI;
    /// <summary></summary>
    [SerializeField] float energy = 1f;
    /// <summary></summary>
    [SerializeField] float randomFactor = .65f;
    [SerializeField] GameObject shadow;

    /// <summary>Player's transform</summary>
    Player player;
    /// <summary>Conductor</summary> 
    Conductor conductor;
    /// <summary></summary>
    AudioSource audio;
    /// <summary></summary>
    Vector3 velocity;
    /// <summary>自分が目標に到達すべき時間</summary>
    private float targetTiming;

    private void Awake()
    {
        conductor = Conductor.Instance;
        player = conductor.Player;
        audio = GetComponent<AudioSource>();
        conductor.SubscribeMagicBullet(this);

        if (!golem)
            golem = GameObject.FindGameObjectWithTag("Golem").GetComponent<Golem>();
    }

    IObservable<Collision> onCollisionEnter;

    private void Start()
    {
        #region FirstAction
        var goalPosition = player.CenterTrackingAnchor.position + Random.insideUnitSphere * randomFactor;
        // もし乱数でz成分がplayerよりも後ろに行った場合, Playerのz成分をpivotにz成分を反転させる
        goalPosition.z = (player.CenterTrackingAnchor.position + player.CenterTrackingAnchor.forward).z;

        var targetVelocity = (goalPosition - transform.position);
        Shot(targetVelocity);
        #endregion

        onCollisionEnter = this.OnCollisionEnterAsObservable();

        // FieldMapと接触した時
        #region OnCollideFieldMap
        onCollisionEnter
            .Where(col => col.gameObject.tag == ObjTag.FieldMap.ToString())
            .Subscribe(col => Explosion())
            .AddTo(this);
        #endregion
        // Handと接触した時
        #region OnTriggerEnterHand
        this.OnTriggerEnterAsObservable()
        .Where(col => col.CompareTag(ObjTag.MagicHand.ToString()))
        .Select(col => col.GetComponent<MagicHand>())
        .Subscribe(OnTriggerHand)
        .AddTo(this);
        #endregion
        #region Golem
        this.OnTriggerEnterAsObservable()
            .Where(col => col.tag == GolemTag.GolemBody.ToString()
            || col.tag == GolemTag.GolemLeftArm.ToString()
            || col.tag == GolemTag.GolemRightArm.ToString())
            .Subscribe(col =>
            {
                switch (col.gameObject.tag)
                {
                    case "GolemBody":
                        break;
                    case "GolemLeftArm":
                        break;
                    case "GolemRightArm":
                        break;
                }
                golem.SetEnergy(energy);
                //ScoreManager.Instance.AddScore(NotesStatus.Perfect);
                Explosion();
            })
            .AddTo(this);
        #endregion
        #region FixedUpdate
        var fixedUpdate = this.FixedUpdateAsObservable();

        //velocity代入
        fixedUpdate
            .Subscribe(_ => transform.position += velocity)
                .AddTo(this);

        // 進行方向に向かせる
        fixedUpdate
            .Select(_ => transform.position)
                .Pairwise((prev, current) => current - prev)
                .Where(v => v.magnitude > Vector3.kEpsilon)
                .Subscribe(dir =>
                {
                    transform.rotation = Quaternion.LookRotation(dir);
                    Debug.DrawLine(transform.position, transform.position + transform.forward * 3);
                })
                .AddTo(this);

        //this.UpdateAsObservable()
        //    .Where(_=>targetTiming - GameManager.Instance.gameObject < 0)
        #endregion

        var update = this.UpdateAsObservable();
        var diff = 0f;

        update
            .Subscribe(_ =>
            {
                shadow.transform.position = new Vector3(shadow.transform.position.x, 0.05f, shadow.transform.position.z);
                shadow.transform.rotation = Quaternion.LookRotation(Vector3.up);
                var vec = player.transform.position - transform.position;
                diff = Vector3.Dot(vec, vec);
                //diff = targetTiming - GameManager.Instance.GetCurrentMusicTime();

            })
            .AddTo(this);

        var deadLine = .5f;
        update
            .Where(_ => player.transform.position.z - deadLine > transform.position.z)
            .Subscribe(_ =>
            {
                ScoreManager.Instance.AddScore(NotesStatus.Bad, identity);
                Destroy(gameObject);
            })
            .AddTo(this);

        //var sm = ScoreManager.Instance;

        //fixedUpdate
        //    .Subscribe(_ =>
        //    {
        //        var vec = player.transform.position - transform.position;
        //        var scolar = Mathf.Abs(Vector3.Dot(vec, vec));
        //        if (scolar < sm.goodUnderLine && scolar > sm.goodUpperLine)
        //        {
        //            var go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        //            go.transform.localScale = Vector3.one * 0.1f;
        //            go.transform.position = transform.position;
        //            var mat = go.GetComponent<Renderer>().material;
        //            if (scolar < sm.perfectUnderLine && scolar > sm.perfectUpperLine)
        //            {
        //                mat.color = Color.red;
        //            }
        //            else
        //                mat.color = Color.white;
        //        }
        //    })
        //    .AddTo(this);
    }

    /// <summary>
    /// 手と衝突した時
    /// </summary>
    /// <param name="magicHand"></param>
    void OnTriggerHand(MagicHand magicHand)
    {
        magicHand.ActivateVibration();
        //手と異なる色だったら
        if (magicHand.Mbs != identity)
        {
            return;
        }

        //var diff = (targetTiming - GameManager.Instance.GetCurrentMusicTime());
        var vec = player.transform.position - transform.position;
        var diff = Vector3.Dot(vec, vec);
        Debug.Log($"diff = {diff}");
        var result = ScoreManager.Instance.JudgeTiming(diff, identity);
        var notesUIObj = Instantiate(
            notesUI
            , notesStatusUISpawnNode.position
            , Quaternion.LookRotation(transform.position - player.CenterTrackingAnchor.position));
        var uiReseiver = notesUIObj.GetComponent<NotesStatusReceiver>();
        uiReseiver.DisplayNotesStatus(result);
        // resultに合わせた音の変更
        switch (result)
        {
            case NotesStatus.Perfect:
                PlaySE(SETag.Shot, PlayStyle.NormalPlay, false);
                break;
            case NotesStatus.Good:
                PlaySE(SETag.Shot, PlayStyle.NormalPlay, false);
                break;
            case NotesStatus.Bad:
                PlaySE(SETag.Shot, PlayStyle.NormalPlay, false);
                break;
            default:
                break;
        }
        var targetVelocity = golem.TargetNodes.ElementAt(Random.Range(0, golem.TargetNodes.Count)).position - magicHand.gameObject.transform.position;
        Shot(targetVelocity);
    }

    /// <summary>
    /// 指定秒数後に目標vectorに到達するvelocityを自身に代入する
    /// </summary>
    public void Shot(Vector3 targetVelocity)
    {
        // Frame per vector = Vector of distance * 1/90(seconds per frame. FPS90.) * Target seconds .
        velocity = targetVelocity * Time.fixedDeltaTime * (1 / conductor.TargetTime);
    }

    /// <summary>
    /// Explosion
    /// </summary>
    void Explosion()
    {
        if (explosionObj)
        {
            Destroy(Instantiate(explosionObj, transform.position, Quaternion.identity), 3f);
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// Initialize instance.
    /// </summary>
    public void InitMB(float targetTiming)
    {
        this.targetTiming = targetTiming;
    }
    /// <summary>
    /// 指定されたSEを再生する. 
    /// </summary>
    /// <param name="clip">再生する音声ファイル</param>
    /// <param name="isDestoryAfterPlaying">再生後自分自身を破壊するか</param>
    /// <param name="playStyle"><see cref="Music.QuantizePlay"/>を使用してQuantizePlayするか</param>
    void PlaySE(AudioClip clip, PlayStyle playStyle, bool isDestoryAfterPlaying)
    {
        audio.clip = clip;
        switch (playStyle)
        {
            case PlayStyle.QuantizePlay:
                if (!audio)
                    Music.QuantizePlay(audio, 1);
                break;
            case PlayStyle.NormalPlay:
                audio?.Play();
                break;
            default:
                break;
        }
        if (isDestoryAfterPlaying)
        {
            var children = GetComponentsInChildren<Transform>();
            foreach (var child in children)
            {
                Destroy(child.gameObject);
            }
            // SE再生後Sound object破棄
            Destroy(gameObject, audio.clip.length / audio.pitch);
        }
    }
    void PlaySE(SETag tag, PlayStyle playStyle, bool isDestoryAfterPlaying)
    {
        switch (tag)
        {
            case SETag.Shot:
                PlaySE(param.Shot, playStyle, isDestoryAfterPlaying);
                break;
            case SETag.Reflection:
                PlaySE(param.Reflection, playStyle, isDestoryAfterPlaying);
                break;
            case SETag.Explosion:
                PlaySE(param.Explosion, playStyle, isDestoryAfterPlaying);
                break;
            case SETag.Bad:
                // TODO:
                break;
            case SETag.None:
            default:
                break;
        }
    }
    private void OnDestroy()
    {
        conductor.UnSubscribeMagicBullet(this);
    }
}

/// <summary>
/// 魔法識別子
/// </summary>
public enum MagicBulletSpecifier
{
    /// <summary>青い魔法</summary>
    RedMagic,
    /// <summary>赤い魔法</summary>
    BlueMagic,
    /// <summary>弾けない魔法</summary>
    NotBounceBackMagic,
    /// <summary>両手の時弾ける魔法</summary>
    BothMagic,
}

/// <summary>
/// Play style on SE
/// </summary>
public enum PlayStyle
{
    QuantizePlay,
    NormalPlay,
}

public enum SETag
{
    None,
    Shot,
    Reflection,
    Explosion,
    Bad,
}