﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class MagicObjParameter
{
    public int Power { get => power; set => power = value; }
    public AudioClip Shot => shot;
    public AudioClip Reflection => reflection;
    public AudioClip Explosion => explosion;

    [SerializeField] int power;
    [SerializeField] AudioClip shot;
    [SerializeField] AudioClip reflection;
    [SerializeField] AudioClip explosion;
}