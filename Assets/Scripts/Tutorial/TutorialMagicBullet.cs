﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

/// <summary>
/// 
/// </summary>
public class TutorialMagicBullet : MonoBehaviour
{
    Transform targetDoor;
    /// <summary>自分自身の魔法の種類</summary>
    [SerializeField] MagicBulletSpecifier identity;
    /// <summary>魔法のパラメータ</summary>
    [SerializeField] MagicObjParameter param;
    /// <summary>Object of  explosion effect</summary>
    [SerializeField] GameObject explosionObj;
    /// <summary></summary>
    [SerializeField] float speed = 1f;
    /// <summary></summary>
    Vector3 velocity;
    /// <summary></summary>
    AudioSource audio;
    [SerializeField] float scalingSpeed = 0.5f;

    private void Awake()
    {
        audio = GetComponent<AudioSource>();
    }

    private void Start()
    {
        transform.localScale = Vector3.zero;


        var onTriggerEnter = this.OnTriggerEnterAsObservable();

        onTriggerEnter
            .Where(col => col.gameObject.tag == ObjTag.FieldMap.ToString())
            .Subscribe(col =>
            {
                Explosion();
            })
            .AddTo(this);

        onTriggerEnter
            .Where(col => col.gameObject.tag == ObjTag.Door.ToString())
            .Subscribe(col =>
            {
                var door = col.gameObject.GetComponent<Door>();
                door.ActivateDoor();
                Explosion();
            })
            .AddTo(this);

        onTriggerEnter
            .Where(col => col.gameObject.tag == ObjTag.MagicHand.ToString())
            .Select(col => col.gameObject.GetComponent<MagicHand>())
            .Subscribe(mh =>
            {
                if (mh.Mbs == identity)
                {
                    //var targetVelocity = (transform.position - mh.gameObject.transform.position).normalized;
                    var targetVelocity = (targetDoor.position - transform.position).normalized;
                    Shot(targetVelocity, SETag.Shot, PlayStyle.NormalPlay);
                }
            })
            .AddTo(this);

        #region Update
        var update = this.UpdateAsObservable();

        var element = 0f;
        update
            .Where(_ => transform.localScale.sqrMagnitude < Vector3.one.sqrMagnitude)
            .Subscribe(_ =>
            {
                element += Time.deltaTime * scalingSpeed;
                transform.localScale = new Vector3(element, element, element);
                if (transform.localScale.sqrMagnitude >= Vector3.one.sqrMagnitude)
                {
                    transform.localScale = Vector3.one;
                }
            })
            .AddTo(this);

        //velocity代入
        update
            .Subscribe(_ => transform.position += velocity)
                .AddTo(this);

        // 進行方向に向かせる
        update
            .Select(_ => transform.position)
                .Pairwise((prev, current) => current - prev)
                .Where(v => v.magnitude > Vector3.kEpsilon)
                .Subscribe(dir =>
                {
                    transform.rotation = Quaternion.LookRotation(dir);
                    Debug.DrawLine(transform.position, transform.position + transform.forward * 3);
                })
                .AddTo(this);
        #endregion
    }

    public void SetTargetDoor(Transform target)
    {
        targetDoor = target;
    }

    /// <summary>
    /// 指定されたSEを再生する. 
    /// </summary>
    /// <param name="clip">再生する音声ファイル</param>
    /// <param name="isDestoryAfterPlaying">再生後自分自身を破壊するか</param>
    /// <param name="playStyle"><see cref="Music.QuantizePlay"/>を使用してQuantizePlayするか</param>
    void PlaySE(AudioClip clip, PlayStyle playStyle, bool isDestoryAfterPlaying)
    {
        audio.clip = clip;
        switch (playStyle)
        {
            case PlayStyle.QuantizePlay:
                Music.QuantizePlay(audio, 1);
                break;
            case PlayStyle.NormalPlay:
                audio.Play();
                break;
            default:
                break;
        }
        if (isDestoryAfterPlaying)
        {
            var children = GetComponentsInChildren<Transform>();
            foreach (var child in children)
            {
                Destroy(child.gameObject);
            }
            // SE再生後Sound object破棄
            Destroy(gameObject, audio.clip.length / audio.pitch);
        }
    }
    void PlaySE(SETag tag, PlayStyle playStyle, bool isDestoryAfterPlaying)
    {
        switch (tag)
        {
            case SETag.Shot:
                PlaySE(param.Shot, playStyle, isDestoryAfterPlaying);
                break;
            case SETag.Reflection:
                PlaySE(param.Reflection, playStyle, isDestoryAfterPlaying);
                break;
            case SETag.Explosion:
                PlaySE(param.Explosion, playStyle, isDestoryAfterPlaying);
                break;
            case SETag.None:
            default:
                break;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public void Shot(Vector3 targetVelocity, SETag tag, PlayStyle playStyle)
    {
        PlaySE(tag, playStyle, false);
        velocity = targetVelocity * speed;
    }
    /// <summary>
    /// Explosion
    /// </summary>
    void Explosion()
    {
        if (explosionObj)
        {
            Instantiate(explosionObj, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
