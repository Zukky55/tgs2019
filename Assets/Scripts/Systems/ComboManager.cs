﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComboManager : MonoSingleton<ComboManager>
{
    /// <summary>現在のコンボ数</summary>
    public uint CurrentCombo { get; set; } = 0;
    /// <summary>最大コンボ数</summary>
    public uint MaxCombo { get; set; } = 0;
    [HideInInspector] public float comboMagnification = 1;

    /// <summary>
    /// 最大コンボを更新する
    /// </summary>
    public void UpdateMaxCombo()
    {
        if(CurrentCombo > MaxCombo)
        {
            MaxCombo = CurrentCombo;
        }
    }

    /// <summary>
    /// 現在のコンボ数に応じた倍率を取得する
    /// </summary>
    /// <returns></returns>
    public float GetComboMagnification(int currentCombo)
    {
        if(1 <= currentCombo && currentCombo < 10)
        {
            comboMagnification = 1;
        }
        else if(10 <= currentCombo && currentCombo < 20)
        {
            comboMagnification = 2;
        }
        else if(20 <= currentCombo && currentCombo < 30)
        {
            comboMagnification = 4;
        }
        else if(30 <= currentCombo && currentCombo < 40)
        {
            comboMagnification = 8;
        }
        else if(40 <= currentCombo && currentCombo < 50)
        {
            comboMagnification = 16;
        }
        else if(50 <= currentCombo && currentCombo < 100)
        {
            comboMagnification = 32;
        }
        else if(100 <= currentCombo)
        {
            comboMagnification = 64;
        }

        return comboMagnification;
    }

}
