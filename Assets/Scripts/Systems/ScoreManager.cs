﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using System;
using UnityEngine.UI;

public class ScoreManager : MonoSingleton<ScoreManager>
{
    public enum Rank
    {
        S,
        A,
        B,
        C,
        D,
    }

    /// <summary>現在の合計スコア</summary>
    public int TotalScore { get; set; } = 0;
    /// <summary>GoodまたはPerfectの判定数</summary>
    public int SuccessCount { get; set; } = 0;
    /// <summary>ノーツの総数</summary>
    public int AllNotes { get; set; } = 0;
    /// <summary>弾けるノーツの総数</summary>
    public int BounceNotes { get; set; } = 0;
    /// <summary>ランク</summary>
    public Rank ResultRank { get => rank; private set => rank = value; }
    /// <summary>フルスコア</summary>
    public int FullScore { get; set; } = 0;
    public float GoodUnderLine => goodUnderLine;
    public float PerfectUnderLine => perfectUnderLine;

    [SerializeField] Rank rank = Rank.D;
    /// <summary>ベースとなる加点スコア</summary>
    [SerializeField] int baseAddScore = 1000;
    /// <summary>Goodステータスの倍率</summary>
    [SerializeField] float goodMagnification = 1f;
    /// <summary>ステータスの倍率</summary>
    [SerializeField] float perfectMagnification = 1.5f;
    /// <summary>Goodになるunder line</summary>
    [SerializeField] public float goodUnderLine;
    /// <summary>Goodになるupper line</summary>
    [SerializeField] public float goodUpperLine;
    /// <summary>Perfectになるunder line</summary>
    [SerializeField] public float perfectUnderLine;
    /// <summary>Perfectになるupper line</summary>
    [SerializeField] public float perfectUpperLine;


    ComboManager comboManager;
    [SerializeField] Text currentScoreText;
    [SerializeField] Text totalScoreText;
    [SerializeField] Text curremtComboText;
    [SerializeField] Text maxComboText;
    /// <summary>成功率を表示するテキスト</summary>
    [SerializeField] Text successRateText;
    [SerializeField] Text rankText;
    [SerializeField] Text thankyouText;
    MusicScoreLoader scoreLoader;
    [SerializeField] GameObject resultCanvas;
    [SerializeField] GameObject scoreCanvas;
    [SerializeField] GameObject comboCanvas;
    [SerializeField] Color redTextColor;
    [SerializeField] Color blueTextColor;

    GameObject player;
    SoundManager soundManager;
    TypefaceAnimator scoreTextAnimator;
    TypefaceAnimator comboTextAnimator;
    private void Start()
    {
        player = GameObject.FindGameObjectWithTag(ObjTag.Player.ToString());
        scoreLoader = MusicScoreLoader.Instance;
        comboManager = ComboManager.Instance;
        soundManager = SoundManager.Instance;
        resultCanvas.SetActive(false);
        scoreCanvas.SetActive(false);
        comboCanvas.SetActive(false);
        scoreTextAnimator = currentScoreText.GetComponent<TypefaceAnimator>();
        comboTextAnimator = curremtComboText.GetComponent<TypefaceAnimator>();
    }

    public void InitializeUICanvas()
    {
        scoreCanvas.SetActive(true);
        BillboardObject.Billboard(ref scoreCanvas, player);
        comboCanvas.SetActive(true);
        BillboardObject.Billboard(ref comboCanvas, player);
        currentScoreText.text = "0";
        curremtComboText.text = "0";
        totalScoreText.enabled = false;
        successRateText.enabled = false;
        rankText.enabled = false;
        thankyouText.enabled = false;
        maxComboText.enabled = false;
    }

    /// <summary>現在のコンボ数に応じたスコアを加算する</summary>
    public void AddScore(NotesStatus status, MagicBulletSpecifier magicBulletSpecifier)
    {
        switch (status)
        {
            case NotesStatus.Perfect:
                SuccessCount++;
                comboManager.CurrentCombo++;
                comboManager.UpdateMaxCombo();
                TotalScore += Mathf.RoundToInt(baseAddScore * perfectMagnification * comboManager.GetComboMagnification((int)comboManager.CurrentCombo));//ベース加算スコア*Perfect倍率*コンボ倍率
                break;
            case NotesStatus.Good:
                SuccessCount++;
                comboManager.CurrentCombo++;
                comboManager.UpdateMaxCombo();
                TotalScore += Mathf.RoundToInt(baseAddScore * goodMagnification * comboManager.GetComboMagnification((int)comboManager.CurrentCombo));
                break;
            case NotesStatus.Bad:
                comboManager.CurrentCombo = 0;//加算せずコンボをリセットする
                break;
            default:
                break;
        }
        currentScoreText.text = TotalScore.ToString();
        curremtComboText.text = comboManager.CurrentCombo.ToString();
        var color = redTextColor;
        if (magicBulletSpecifier.HasFlag(MagicBulletSpecifier.BlueMagic))
        {
            Debug.Log($" 通ったらしいぜ");
            color = blueTextColor;
        }
        scoreTextAnimator.colorFrom = color;
        comboTextAnimator.colorFrom = color;
        scoreTextAnimator.Play();
        comboTextAnimator.Play();
        //Debug.Log($"合計スコア={TotalScore},コンボ数={comboManager.CurrentCombo},最大コンボ={comboManager.MaxCombo}");
    }

    /// <summary>
    /// スコアとコンボUIを非表示にする
    /// </summary>
    public void NonActiveScoreCanvas()
    {
        scoreCanvas.SetActive(false);
        comboCanvas.SetActive(false);
    }


    /// <summary>
    /// リザルトUIを表示する
    /// </summary>
    /// <param name="animationSpan"></param>
    /// <returns></returns>
    public IEnumerator OnSetupResultUI(float animationSpan = 1)
    {
        resultCanvas.SetActive(true);
        BillboardObject.Billboard(ref resultCanvas, player);


        yield return new WaitForSeconds(animationSpan);
        float successRate = (float)SuccessCount / (float)BounceNotes * 100f;//獲得スコアの割合を算出
        Debug.Log($"{SuccessCount}/{BounceNotes}*100");
        successRateText.text = successRate.ToString("f0") + "%";
        successRateText.enabled = true;

        yield return new WaitForSeconds(animationSpan);
        maxComboText.text = comboManager.MaxCombo.ToString();
        maxComboText.enabled = true;

        yield return new WaitForSeconds(animationSpan);
        totalScoreText.text = TotalScore.ToString();
        totalScoreText.enabled = true;

        yield return new WaitForSeconds(animationSpan);
        rankText.text = GetResultRank(successRate).ToString();
        rankText.enabled = true;

        yield return new WaitForSeconds(animationSpan);
        thankyouText.enabled = true;
    }

    /// <summary>フルスコアを算出する</summary>
    public void CalculationFullScore()
    {
        for (int i = 1; i <= scoreLoader.timings.Count; i++)
        {
            FullScore += Mathf.RoundToInt(baseAddScore * perfectMagnification * comboManager.GetComboMagnification(i));
        }
        Debug.Log($"フルスコア：{FullScore}");
    }

    public Rank GetResultRank(float successRate)
    {
        Rank temp = Rank.D;

        if (0 <= successRate && successRate <= 20)
        {
            temp = Rank.D;
        }
        else if (21 <= successRate && successRate <= 40)
        {
            temp = Rank.C;
        }
        else if (41 <= successRate && successRate <= 60)
        {
            temp = Rank.B;
        }
        else if (61 <= successRate && successRate <= 80)
        {
            temp = Rank.A;
        }
        else if (81 <= successRate)
        {
            temp = Rank.S;
        }
        return temp;
    }

    /// <summary>
    /// タイミングの判定をして,スコア計算をする
    /// </summary>
    /// <param name="differenceDistance"></param>
    /// <returns></returns>
    public NotesStatus JudgeTiming(float differenceDistance, MagicBulletSpecifier mbs)
    {
        differenceDistance = Mathf.Abs(differenceDistance);

        if (differenceDistance < goodUnderLine && differenceDistance > goodUpperLine)
        {
            if (differenceDistance < perfectUnderLine && differenceDistance > perfectUpperLine)
            {
                AddScore(NotesStatus.Perfect, mbs);
                return NotesStatus.Perfect;
            }
            AddScore(NotesStatus.Good, mbs);
            return NotesStatus.Good;
        }
        AddScore(NotesStatus.Bad, mbs);
        return NotesStatus.Bad;
    }
}
public enum NotesStatus
{
    Perfect,
    Good,
    Bad,
}
