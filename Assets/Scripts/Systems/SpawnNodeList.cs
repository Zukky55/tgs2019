﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SpawnNodeList : MonoBehaviour
{
    [SerializeField] List<SpawnPosition> spawnPositions;
    public List<SpawnPosition> SpawnNodes { get { return spawnPositions; } }
}
/// <summary>生成位置</summary>
[Serializable]
public class SpawnPosition
{
    [SerializeField] SpawnNode spawnNode;
    [SerializeField] GameObject spawnPoint;

    /// <summary>生成位置ノード</summary>
    public SpawnNode SpawnNode { get { return spawnNode; } }

    /// <summary>生成ポイントオブジェクトの座標</summary>
    public Vector3 SpawnPoint
    {
        get
        {
            Vector3 temp = spawnPoint.transform.position;
            return temp;
        }
    }
}
