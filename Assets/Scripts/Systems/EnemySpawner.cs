﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UniRx;
using UniRx.Triggers;

/// <summary>
/// 敵を適切に召喚する役割
/// </summary>
public class EnemySpawner : MonoSingleton<EnemySpawner>
{
    [SerializeField] SpawnNodeList spawnNodeList;
    [SerializeField] EnemyTypeList enemyTypeList;
    [SerializeField] EffectList effectList;
    /// <summary>
    /// 指定された場所に指定された敵を召喚する
    /// </summary>
    /// <param name="nodeSpecified">座標指定子</param>
    /// <param name="enemySpecified">召喚対象指定子</param>
    /// <returns>召喚された敵の参照</returns>
    public Enemy SpawnEnemy(SpawnNode nodeSpecified, EnemySpecifier enemySpecified)
    {
        SpawnNode spawnNode = SpawnNode.L_1;
        bool isTarget = false;

        switch (nodeSpecified)
        {
            case SpawnNode.L_1:
                spawnNode = SpawnNode.L_1;
                break;
            case SpawnNode.R_1:
                spawnNode = SpawnNode.R_1;
                break;
            case SpawnNode.L_3:
                spawnNode = SpawnNode.L_3;
                break;
            case SpawnNode.R_3:
                spawnNode = SpawnNode.R_3;
                break;
            case SpawnNode.R_2:
                spawnNode = SpawnNode.R_2;
                break;
            case SpawnNode.L_2:
                spawnNode = SpawnNode.L_2;
                break;
            case SpawnNode.Center:
                spawnNode = SpawnNode.Center;
                isTarget = true;//中央に配置する敵はターゲットエネミーとする
                break;
            default:
                break;
        }
        EnemySpecifier enemySpecifier = EnemySpecifier.Dangoro;
        switch (enemySpecified)
        {
            case EnemySpecifier.Dangoro:
                enemySpecifier = EnemySpecifier.Dangoro;
                break;
            case EnemySpecifier.Shokeke:
                enemySpecifier = EnemySpecifier.Shokeke;
                break;
            case EnemySpecifier.Dohtakun:
                enemySpecifier = EnemySpecifier.Dohtakun;
                break;
            case EnemySpecifier.Golem:
                enemySpecifier = EnemySpecifier.Golem;
                break;
            default:
                break;
        }
        EnemyType enemyType = null;

        if (!isTarget)
        {
            enemyType = enemyTypeList.AttackEnemyTypes.Find(enemy => enemy.EnemySpecifier == enemySpecifier);
        }
        else
        {
            enemyType = enemyTypeList.TargetEnemyTypes.Find(enemy => enemy.EnemySpecifier == enemySpecifier);
        }
        SpawnPosition spawnPosition = spawnNodeList.SpawnNodes.Find(spa => spa.SpawnNode == spawnNode);
        GameObject obj = Instantiate(enemyType.Enemy, spawnPosition.SpawnPoint, Quaternion.identity);
        var effectMat = effectList.EffectMaterialList.Find(e => e.TargetEnemy == enemySpecified);
        var rot = Quaternion.AngleAxis(-90, Vector3.right);
        Instantiate(effectMat.Effect, spawnPosition.SpawnPoint, rot);
        return obj.GetComponent<Enemy>();
    }
}

#region
/// <summary>
/// 敵を召喚する座標指定子
/// </summary>
public enum SpawnNode
{
    /// <summary>左ダンゴロ</summary>
    L_1,
    /// <summary>左ショケケ</summary>
    L_2,
    /// <summary>左ドータクン</summary>
    L_3,
    /// <summary>右ダンゴロ</summary>
    R_1,
    /// <summary>中ショケケ</summary>
    R_2,
    /// <summary>右ドータクン</summary>
    R_3,
    /// <summary>中央</summary>
    Center,
}

/// <summary>
/// 召喚する敵の指定子
/// </summary>
public enum EnemySpecifier
{
    Dangoro,
    Shokeke,
    Dohtakun,
    Golem,
}
#endregion