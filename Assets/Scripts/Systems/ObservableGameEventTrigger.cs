﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Linq;
using UniRx;
using UniRx.Triggers;

namespace UniRx.Triggers
{
    [DisallowMultipleComponent]
    public class ObservableGameEventTrigger : ObservableTriggerBase
    {
        #region Events
        public static Subject<GameEvent> gameEvent { get; set; }
        static GameEvent currentEvent;

        /// <summary>
        /// 現在進行しているイベント
        /// </summary>
        public static GameEvent CurrentEvent
        {
            get => currentEvent;
            private set
            {
                currentEvent = value;
                gameEvent?.OnNext(currentEvent);
            }
        }
        #endregion
        #region Wave
        Subject<Wave> wave;
        Wave currentWave;

        public Wave CurrentWave
        {
            get => currentWave;
            private set
            {
                currentWave = value;
                wave?.OnNext(currentWave);
            }
        }
        #endregion
        [SerializeField] List<Wave> waves;



        /// <summary>
        /// イベントが変更されたフレームにコールバックするIObservable<T>を返す
        /// </summary>
        /// <returns></returns>
        public IObservable<GameEvent> OnGameEventAsObservable()
        {
            Observable.Empty<GameEvent>();
            return gameEvent ?? (gameEvent = new Subject<GameEvent>());
        }
        public static void SetGameEvent(GameEvent ge) => CurrentEvent = ge;
        public void SetWave(Wave nextWave) => CurrentWave = nextWave;

        private void OnDestroy()
        {
            Destroy(this);
        }

        private void Start()
        {
            //var waveObservable = new Subject<Wave>();
            //waveObservable.SkipWhile(wave => wave.IsWavesEnd)
            //    .Subscribe() //TODO: eventを組み込む
            //    .AddTo(this);
            //FPSCounter.Current.Subscribe(fps => Debug.Log(fps));
        }

        /// <summary>
        /// Dispose処理?多分
        /// </summary>
        protected override void RaiseOnCompletedOnDestroy()
        {
            if(gameEvent != null)
            {
                gameEvent.OnCompleted();
            }
        }
        /// <summary>
        /// ゲームの各イベント
        /// </summary>
        public enum GameEvent
        {
            GetTheWeapon,
            DisplayTitle,
            OpenStoneDoor,
            PlayerMoveToForward,
            Stage,
            StageClearEvent,
            Result,
            MoveToTheExit,
            GameEnd,
            GameOver,
            TutorialEvent,
        }
    }
}
