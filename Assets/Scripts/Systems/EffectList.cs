﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Linq;
using UniRx;
using UniRx.Triggers;
using UnityEngine.UI;

[Serializable]
[CreateAssetMenu(fileName = "EffectList", menuName = "EffectList")]
public class EffectList : ScriptableObject
{
    public List<EffectMaterial> EffectMaterialList { get => effects; set => effects = value; }
    [SerializeField] List<EffectMaterial> effects;

    [Serializable]
    public class EffectMaterial
    {
        public EnemySpecifier TargetEnemy;
        public GameObject Effect;
    }
}

