﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EnemyTypeList : MonoBehaviour
{
    [SerializeField,Header("攻撃専用の敵リスト")] List<EnemyType> attackEnemyTypes;
    [SerializeField,Header("プレーヤーが狙う敵リスト")] List<EnemyType> targetEnemyTypes;

    public List<EnemyType> AttackEnemyTypes { get { return attackEnemyTypes; } }
    public List<EnemyType> TargetEnemyTypes { get { return targetEnemyTypes; } }
}
/// <summary>敵の種類</summary>
[Serializable]
public class EnemyType
{
    [SerializeField] EnemySpecifier enemySpecifier;
    [SerializeField] GameObject enemy;

    /// <summary>敵識別ID</summary>
    public EnemySpecifier EnemySpecifier { get { return enemySpecifier; } }
    /// <summary>敵オブジェクト</summary>
    public GameObject Enemy { get { return enemy; } }
}
