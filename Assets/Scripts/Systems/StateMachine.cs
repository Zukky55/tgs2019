﻿using UnityEngine;
using UniRx;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UniRx.Triggers;

public class StateMachine : MonoBehaviour
{
    [Serializable]
    public class State
    {
        public StateTag MyProperty { get; set; }
    }

    private State current;

    public State Current
    {
        get { return current; }
        set { current = value; }
    }

    public enum StateTag
    {
        GetTheWeapon,
        OpenStoneDoor,
        
    }
}
