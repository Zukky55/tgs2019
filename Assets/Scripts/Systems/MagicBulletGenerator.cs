﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicBulletGenerator : MonoSingleton<MagicBulletGenerator>
{
    [SerializeField] GameObject redMagic;
    [SerializeField] GameObject blueMagic;
    [SerializeField] GameObject bothMagic;
    [SerializeField] GameObject notBounceMagic;
    public List<GameObject> magicList = new List<GameObject>();

    private void Start()
    {
        magicList.Clear();
        GenerateMagicBullet(redMagic,10);
        GenerateMagicBullet(blueMagic,10);
        GenerateMagicBullet(bothMagic,10);
        GenerateMagicBullet(notBounceMagic,10);

    }

    void GenerateMagicBullet(GameObject magicBullet,int generate)
    {
        GameObject obj = null;
        for(int i = 0; i < generate; i++)
        {
            obj = Instantiate(magicBullet);
            obj.SetActive(false);
            magicList.Add(obj);
        }
    }

    public GameObject GetMagicBullet(MagicBulletSpecifier magicBulletSpecifier)
    {
        GameObject obj = null;
        foreach(var magic in magicList)
        {
            if(!magic.activeSelf && magic.GetComponent<MagicBullet>().Identity == magicBulletSpecifier)
            {
                magic.SetActive(true);
                obj = magic;
                break;
            }
        }
        return obj;
    }
}
