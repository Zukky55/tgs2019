﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Linq;
using UniRx;
using UniRx.Triggers;
namespace UniRx.Triggers
{
    public static class ObservableGameEventTriggerExtention
    {
        public static IObservable<ObservableGameEventTrigger.GameEvent> OnGameEventAsObservable(this GameObject gameObject)
        {
            if (gameObject == null) return Observable.Empty<ObservableGameEventTrigger.GameEvent>();
            return GetOrAddComponent(gameObject).OnGameEventAsObservable();
        }
        public static IObservable<ObservableGameEventTrigger.GameEvent> OnGameEventAsObservable(this Component component)
        {
            if (component == null || component.gameObject == null) return Observable.Empty<ObservableGameEventTrigger.GameEvent>();
            return GetOrAddComponent(component.gameObject).OnGameEventAsObservable();
        }

        static ObservableGameEventTrigger GetOrAddComponent(GameObject gameObject)
        {
            var component = gameObject.GetComponent<ObservableGameEventTrigger>();
            if (!component)
            {
                component = gameObject.AddComponent<ObservableGameEventTrigger>();
            }
            return component;
        }
        static IObservable<ObservableGameEventTrigger.GameEvent> GetOrAddComponent(this Component component)
        {
            if (component == null || component.gameObject == null)
            {
                return Observable.Empty<ObservableGameEventTrigger.GameEvent>();
            }
            return GetOrAddComponent(component.gameObject).OnGameEventAsObservable();
        }
    }
}
