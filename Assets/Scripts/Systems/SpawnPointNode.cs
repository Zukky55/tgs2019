﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UniRx;
using UniRx.Triggers;

public class SpawnPointNode : MonoBehaviour
{
    [SerializeField] List<Node> nodes;

    public List<Node> Nodes => nodes;
    public class Node 
    {
        [SerializeField] SpawnNode nodeSpecifier;

        /// <summary>ノード指定子</summary>
        public SpawnNode NodeSpecifier { get; set; }
    }
}