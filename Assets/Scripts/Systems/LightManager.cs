﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

public class LightManager : MonoBehaviour
{
    ComboManager comboManager;
    private Renderer renderer;
    [SerializeField] Animator FirstOrbAnim;
    [SerializeField] Animator SecondOrbAnim;
    [SerializeField] Animator [] LantanAnim;
    [SerializeField] Animator[] SpectrumAnim;
    [SerializeField] float FirstOrbTime;
    [SerializeField] float SecondOrbTime;
    [SerializeField] float LantanTime;
    float r, b, g = 0;
    float Count = 0;


    //[SerializeField] GameObject light;
    // Start is called before the first frame update
    void Start()
    {
        
        //FirstOrbAnim.GetComponent<Animator>();
        //SecondOrbAnim.GetComponent<Animator>();
        //LantanAnim.GetComponent<Animator>();
        //var FirstOrbAnimState = FirstOrbAnim.GetCurrentAnimatorStateInfo(0);
        //var SecondOrbAnimState = SecondOrbAnim.GetCurrentAnimatorStateInfo(0);
        //var LantanAnimState = LantanAnim.GetCurrentAnimatorStateInfo(0);

        //comboManager = ComboManager.Instance;
        //renderer = GetComponent<Renderer>();
        //var originMaterial = new Material(renderer.material);
        ////renderer.material.SetColor("_EmissionColor", new Color(0, 0, 0));
        this.UpdateAsObservable()

            .Subscribe(_ =>
            {
                var a = FirstOrbAnim.GetCurrentAnimatorStateInfo(0).normalizedTime;

                //var nomalize = FirstOrbAnimState.normalizedTime;
                //float time = Time.time;
                //float val = Mathf.PingPong(Time.time, 1.0f);
                //Color color = new Color(0, 5.0f - val + val, 0);

                //renderer.material.EnableKeyword("_EMISSION");
                //r.material.SetColor("_EmissionColor", color);
                if (Music.IsJustChangedBeat())
                {
                    Count++;
                    if (Count >= FirstOrbTime)
                    {
                        FirstOrbAnim.SetTrigger("ColorTrigger");
                        //FirstOrbAnim.SetTrigger("ColorTrigger");
                    }
                    if (Count >= SecondOrbTime)
                    {
                        SecondOrbAnim.CrossFade("Orb",0,0,a);
                        //SecondOrbAnim.SetTrigger("NextColorTrigger");
                        

                    }
                    if (Count >= LantanTime)
                    {
                        for (int count = 0; count < LantanAnim.Length; count++)
                        {
                            //LantanAnim[count].ForceStateNormalizedTime(a);
                            LantanAnim[count].CrossFade("Orb",0,0,a);
                            
                        }
                        for (int count = 0; count < SpectrumAnim.Length; count++)
                        {
                            //LantanAnim[count].ForceStateNormalizedTime(a);
                           
                            SpectrumAnim[count].CrossFade("TotemPole", 0, 0, a);
                        }

                        //LantanAnim[0].SetTrigger("LantanTrigger");
                        //LantanAnim[1].SetTrigger("LantanTrigger");
                        //LantanAnim[2].SetTrigger("LantanTrigger");
                        //LantanAnim[3].SetTrigger("LantanTrigger");
                        //LantanAnim[4].SetTrigger("LantanTrigger");


                    }
                    //r.material.SetColor("_EmissionColor", new Color(1, 0, 0));
                }
                //if(Time.time >= SecondOrbTime * 0.545454f)
                //{
                //    SecondOrbAnim.SetTrigger("NextColorTrigger");

                //    //r.material = originMaterial;
                //    //r.material.SetColor("_EmissionColor", new Color(0, 0, 0));
                //}
                //if (Time.time >= LantanTime * 0.545454f && FirstOrbAnimState.normalizedTime == 1)
                //{

                //    LantanAnim.SetTrigger("LantanTrigger");
                //    //r.material = originMaterial;
                //    //r.material.SetColor("_EmissionColor", new Color(0, 0, 0));
                //}
            });

    }

    public void ChangeColor(Coloring color)
    {
        //Color fromColor = renderer.material.SetColor;


        float startTime = Time.time;

        //float endTime = Time.time + duration;

        //float marginR = color.r;

        //float marginG = color.g;

        //float marginB = color.b;

        this.UpdateAsObservable()
        .Where(v => Time.time >= FirstOrbTime)
        .Subscribe(_ =>
        {
            switch (color)
            {
                case Coloring.RED:
                    b--;
                    g--;
                    r++;
                    renderer.material.SetColor("_EmissionColor", new Color(r, g, b));
                    break;

                case Coloring.GREEN:
                    b--;
                    r--;
                    g++;
                    renderer.material.SetColor("_EmissionColor", new Color(r, g, b));
                    break;

                case Coloring.BLUE:
                    r--;
                    g--;
                    r++;
                    renderer.material.SetColor("_EmissionColor", new Color(r, g, b));
                    break;


            }
            r++;
            renderer.material.SetColor("_EmissionColor", new Color(r, g, b));
           //fromColor.r = fromColor.r + (Time.deltaTime / duration) * marginR;

           //fromColor.g = fromColor.g + (Time.deltaTime / duration) * marginG;

           //fromColor.b = fromColor.b + (Time.deltaTime / duration) * marginB;

           //renderer.material.SetColor("_EmissionColor", fromColor);

       })
        .AddTo(this);
    }
    public enum Coloring
    {
        RED,
        GREEN,
        BLUE,
    }


    // Update is called once per frame
    void Update()
    {

    }
}
