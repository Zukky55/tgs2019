﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class MusicScoreData
{
    [SerializeField] MusicScoreID musicScoreID;
    [SerializeField] TextAsset musicScoreData;

    public MusicScoreID MusicScoreID { get { return musicScoreID; } }
    public TextAsset MusicScore { get { return musicScoreData; } }
}
