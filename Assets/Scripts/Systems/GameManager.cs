﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using System;
using System.Linq;
using UnityEngine.SceneManagement;

public class GameManager : MonoSingleton<GameManager>
{
    MusicScoreLoader scoreLoader;
    EnemySpawner enemySpawner;
    Conductor conductor;
    EnemySpecifier[] enemySpecifiers;
    MagicBulletSpecifier[] magicBulletTypes;
    [HideInInspector] public List<Enemy> attackEnemyList = new List<Enemy>();
    /// <summary>一番最初に呼ぶイベント</summary>
    [SerializeField] ObservableGameEventTrigger.GameEvent firstEvent;

    [SerializeField] float currentMusicTime;
    int index = 0;
    float startTime = 0;
    public int shotCount = 0;

    const float adjustTime = .1f;
    public void ClearEnemyList()
    {
        attackEnemyList.Clear();
    }

    private void Awake()
    {
        enemySpawner = EnemySpawner.Instance;
        scoreLoader = MusicScoreLoader.Instance;
        conductor = Conductor.Instance;
        Debug.Log(SceneManager.GetActiveScene().name);
    }

    private void Start()
    {
        // Call event
        Observable.Timer(TimeSpan.FromSeconds(2))
            .Subscribe(_ =>
            {
                ObservableGameEventTrigger.SetGameEvent(firstEvent);
            })
            .AddTo(this);

        var update = this.UpdateAsObservable();

        // debug
        update
            .Subscribe(_ => currentMusicTime = GetCurrentMusicTime()).AddTo(this);

        // Restart
        update
            .Where(_ => OVRInput.GetDown(OVRInput.RawButton.Start) || Input.GetKeyDown(KeyCode.R))
            .Subscribe(_ => SceneManager.LoadScene(SceneManager.GetActiveScene().name))
            .AddTo(this);
        Debug.Log("[R]でゲームリスタート");
    }

    /// <summary>指定の敵に指定の魔法を出す指令をする</summary>
    public void CommandToEnemy()
    {
        while (scoreLoader.timings.Count > index &&
            scoreLoader.timings.ElementAt(index) - conductor.TargetTime - adjustTime < GetCurrentMusicTime() &&
            scoreLoader.timings.ElementAt(index) != 0)
        {
            SpawnNode spawnNode = (SpawnNode)scoreLoader.enemyNode.ElementAt(index);
            //Debug.Log(spawnNode.ToString());
            Enemy tempEnemy = attackEnemyList.FirstOrDefault(enemy => enemy.enemySpawnNode == spawnNode);
            if (tempEnemy == null)
            {
                index++;
                break;
            }

            var magicBulletType = (MagicBulletSpecifier)scoreLoader.bulletTypes[index];
            tempEnemy.ShotMagicBullet(magicBulletType, scoreLoader.timings.ElementAt(index) - adjustTime);
            index++;
        }
    }

    public float GetCurrentMusicTime()
    {
        return Time.timeSinceLevelLoad - startTime;
    }

    public void StartMusicTime()
    {
        startTime = Time.timeSinceLevelLoad;
    }
}
