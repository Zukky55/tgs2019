﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UniRx;

public static class FPSCounter
{
    const int Buffersize = 5;
    public static IReadOnlyReactiveProperty<float> Current { get; private set; }

    static FPSCounter()
    {
        Current = Observable.EveryUpdate()
            .Select(_ => Time.deltaTime)
            .Buffer(Buffersize, 1)
            .Select(x => 1f / x.Average())
            .ToReadOnlyReactiveProperty();
    }
}
