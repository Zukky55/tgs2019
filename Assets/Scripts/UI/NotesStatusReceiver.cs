﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using UnityEngine.UI;

public class NotesStatusReceiver : MonoBehaviour
{
    [SerializeField] Image targetImage;
    [SerializeField] Sprite goodTexture;
    [SerializeField] Sprite badTexture;
    [SerializeField] Sprite perfectTexture;

    Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void DisplayNotesStatus(NotesStatus status)
    {
        switch (status)
        {
            case NotesStatus.Perfect:
                targetImage.sprite = perfectTexture;
                break;
            case NotesStatus.Good:
                targetImage.sprite = goodTexture;
                break;
            case NotesStatus.Bad:
                targetImage.sprite = badTexture;
                break;
            default:
                break;
        }
        animator.SetTrigger("DisplayTrigger");
    }

    public void DestroyMySelf() => Destroy(gameObject);
}
