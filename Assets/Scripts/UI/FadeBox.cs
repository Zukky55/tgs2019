﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
public class FadeBox : MonoBehaviour
{
    [SerializeField] Transform targetNode;
    [SerializeField] Transform player;

    Animator fadeBoxAnim;

    private void Awake()
    {
        fadeBoxAnim = GetComponent<Animator>();
    }

    void Start()
    {
        this.OnGameEventAsObservable()
          .Where(gEvent => gEvent == ObservableGameEventTrigger.GameEvent.PlayerMoveToForward)
          .Subscribe(v => FadeOut())
          .AddTo(this);
    }
    public void FadeOut() => fadeBoxAnim.SetTrigger(stateParameter.FadeOut.ToString());
    public void FadeIn()
    {
        player.position = targetNode.position;
        fadeBoxAnim.SetTrigger(stateParameter.FadeIn.ToString());
    }

    public void OnCompleted() => ObservableGameEventTrigger.SetGameEvent(ObservableGameEventTrigger.GameEvent.Stage);
    enum stateParameter
    {
        Idle,
        FadeIn,
        FadeOut,
    }
}
