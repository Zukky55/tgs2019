﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Linq;
using UniRx;
using UniRx.Triggers;
using UnityEngine.UI;

public class ResultEvent : MonoBehaviour
{
    private void Start()
    {
        this.OnGameEventAsObservable()
            .Where(gEvent => gEvent == ObservableGameEventTrigger.GameEvent.Result)
            .Subscribe(v =>
            {
                SoundManager.Instance.PlayBGM(SoundManager.BGM.Result, true);
                StartCoroutine(ScoreManager.Instance.OnSetupResultUI());
            }).AddTo(this);
    }
}
