﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Linq;
using UniRx;
using UniRx.Triggers;

/// <summary>
/// ゲーム開始するときに呼ばれるイベント
/// </summary>
public class PlayGameEvent : MonoBehaviour
{
    [SerializeField, Header("敵が生成されてからゲーム開始するまでの時間")] float waitStartTime = 3;
    GameManager gameManager;
    EnemySpawner enemySpawner;
    MusicScoreLoader musicScoreLoader;
    SoundManager soundManager;
    [SerializeField] float addEnemyTiming1 = 35;
    [SerializeField] float addEnemyTiming2 = 73;
    [SerializeField] float addEnemyTiming3 = 73;
    [SerializeField] List<Audience> audiences;

    // IObservable<ObservableGameEventTrigger.GameEvent> gameEvent;
    private void Start()
    {
        gameManager = GameManager.Instance;
        enemySpawner = EnemySpawner.Instance;
        musicScoreLoader = MusicScoreLoader.Instance;
        soundManager = SoundManager.Instance;

        this.OnGameEventAsObservable()
          .Where(gEvent => gEvent == ObservableGameEventTrigger.GameEvent.Stage)
            .Subscribe(v =>
            {
                ScoreManager.Instance.InitializeUICanvas();
                Observable.Timer(TimeSpan.FromSeconds(waitStartTime))
            .Subscribe(e =>
            {
                musicScoreLoader.InitializeArray();
                musicScoreLoader.LoadMusicScore(MusicScoreID.Stage);
                enemySpawner.SpawnEnemy(SpawnNode.Center, EnemySpecifier.Golem);
                gameManager.ClearEnemyList();//list宣言時に要素が1つある状態で生成されるため全ての要素を削除する

                var enemy1 = enemySpawner.SpawnEnemy(SpawnNode.L_1, EnemySpecifier.Dangoro);
                enemy1.SetSpawnNode(SpawnNode.L_1);
                var enemy2 = enemySpawner.SpawnEnemy(SpawnNode.R_1, EnemySpecifier.Dangoro);
                enemy2.SetSpawnNode(SpawnNode.R_1);


                gameManager.attackEnemyList.Add(enemy1);
                gameManager.attackEnemyList.Add(enemy2);

                soundManager.PlayBGM(SoundManager.BGM.Stage, false);

                this.UpdateAsObservable()
                .Where(o => soundManager.isPlaying)
                 .Subscribe(o => gameManager.CommandToEnemy())//曲再生中に指令を出す
                 .AddTo(this);

                var enemy3 = enemySpawner.SpawnEnemy(SpawnNode.L_2, EnemySpecifier.Shokeke);
                enemy3.SetSpawnNode(SpawnNode.L_2);
                var enemy4 = enemySpawner.SpawnEnemy(SpawnNode.R_2, EnemySpecifier.Shokeke);
                enemy4.SetSpawnNode(SpawnNode.R_2);
                var enemy5 = enemySpawner.SpawnEnemy(SpawnNode.L_3, EnemySpecifier.Dohtakun);
                enemy5.SetSpawnNode(SpawnNode.L_3);
                var enemy6 = enemySpawner.SpawnEnemy(SpawnNode.R_3, EnemySpecifier.Dohtakun);
                enemy6.SetSpawnNode(SpawnNode.R_3);

                gameManager.attackEnemyList.Add(enemy3);
                gameManager.attackEnemyList.Add(enemy4);
                gameManager.attackEnemyList.Add(enemy5);
                gameManager.attackEnemyList.Add(enemy6);

                Observable.Timer(TimeSpan.FromSeconds(addEnemyTiming1))
                       .Subscribe(o =>
                       {
                           SpawnAudience(EnemySpecifier.Dangoro);
                       })
                       .AddTo(this);

                Observable.Timer(TimeSpan.FromSeconds(addEnemyTiming2))
                   .Subscribe(o =>
                   {
                       SpawnAudience(EnemySpecifier.Shokeke);
                   })
                   .AddTo(this);
                Observable.Timer(TimeSpan.FromSeconds(addEnemyTiming3))
                   .Subscribe(o =>
                   {
                       SpawnAudience(EnemySpecifier.Dohtakun);
                   })
                   .AddTo(this);

            }).AddTo(this);
            }).AddTo(this);
    }

    void SpawnAudience(EnemySpecifier specifier)
    {
        foreach (var audience in audiences)
        {
            if (audience.Identity == specifier)
            {
                audience.gameObject.SetActive(true);
            }
        }
    }
}
