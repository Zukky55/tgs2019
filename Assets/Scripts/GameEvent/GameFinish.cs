﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using UniRx;
using UniRx.Triggers;
public class GameFinish : MonoBehaviour
{
    [SerializeField] DoorAnimation door;
    [SerializeField] Transform PlayerPos;
    [SerializeField] Vector3 SetPos;
    [SerializeField] float Speed;
    // Start is called before the first frame update
    void Start()
    {
        this.OnGameEventAsObservable()
          .Where(gEvent => gEvent == ObservableGameEventTrigger.GameEvent.GameEnd)
          .Subscribe(v =>
          {
              MoveBehind(PlayerPos, Speed);
              Observable.Timer(TimeSpan.FromSeconds(9))
                  .Subscribe(_ => door.CloseDoor())
                  .AddTo(this);

          })
          .AddTo(this);
    }

    private void MoveBehind(Transform NowPos, float speed)
    {
        //var fixed_ = Observable.EveryFixedUpdate()
        //    .Where(a => NowPos.position.z >= -3);
        //this.FixedUpdateAsObservable()
        // .TakeUntil(fixed_)
        var disposable = new SingleAssignmentDisposable();
        disposable.Disposable = Observable.EveryFixedUpdate()
        .Subscribe(_ =>
        {
            float step = speed * Time.deltaTime;
            NowPos.position = Vector3.MoveTowards(NowPos.position, SetPos, step);
            if (NowPos.position.z <= -3)
            {
                disposable.Dispose();
            }
        });
        //.AddTo(this);
    }

}