﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UniRx;
using UniRx.Triggers;
using System;
public class OpenStoneDoor : MonoBehaviour
{
    [SerializeField] DoorAnimation door;
    [SerializeField] float delayTime = 1f;

    // Start is called before the first frame update
    void Start()
    {
        this.OnGameEventAsObservable()
           .Where(gEvent => gEvent == ObservableGameEventTrigger.GameEvent.OpenStoneDoor)
           //.Delay(TimeSpan.FromSeconds(delayTime))
           .Subscribe(v =>
           {
               door.OpenDoor();
               Observable.Timer(TimeSpan.FromSeconds(10))
                   .Subscribe(_ =>
                   {
                       ObservableGameEventTrigger.SetGameEvent(ObservableGameEventTrigger.GameEvent.PlayerMoveToForward);
                   })
                   .AddTo(this);
           })
           .AddTo(this);
    }

}
