﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Linq;
using UniRx;
using UniRx.Triggers;
using UnityEngine.UI;

public class HitTheMagicEvent : MonoBehaviour
{
    [SerializeField] GameObject blueMagicBullet;
    [SerializeField] GameObject redMagicBullet;
    [SerializeField] Text textBox;
    [TextArea(0,3)]
    [SerializeField] string script;
    private void Start()
    {
        this.OnGameEventAsObservable()
            .Where(gEvent => gEvent == ObservableGameEventTrigger.GameEvent.DisplayTitle)
            .Subscribe(v =>
            {

            })
            .AddTo(this);
    }
}
