﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Linq;
using UniRx;
using UniRx.Triggers;
using UnityEngine.UI;
/// <summary>
/// 
/// </summary>
public class GetTheWeaponEvent : MonoBehaviour
{
    [SerializeField] Transform l_bracelet;
    [SerializeField] Transform r_bracelet;
    [SerializeField] Transform LeftHand;
    [SerializeField] Transform RightHand;
    /// <summary>移動補間値</summary>
    [SerializeField] float moveSpeed = 3f;
    /// <summary>手を検知する範囲</summary>
    [SerializeField] float handDetectionRange = .5f;
    /// <summary>次のイベント開始の遅延時間</summary>
    [SerializeField] float delayTimeForNextEventStart = 3;
    [SerializeField] Transform targetRightBracelet;
    [SerializeField] Transform targetLeftBracelet;
    [SerializeField] Text Tutorial;
    [SerializeField] float fadingSpeed = 1f;
    [SerializeField] float delayTimeOfTextDisplayAnimation = 2f;
    float textAplha = 0;
    bool textAlphaFlag = true;
    /// <summary>右手用リングの右手からの相対的な装着位置</summary>
    readonly Vector3 rightRingPositionOffset = new Vector3(0.056f, -0.015f, -0.09f);
    /// <summary>左手用リングの左手からの相対的な装着位置</summary>
    readonly Vector3 leftRingPositionOffset = new Vector3(-0.056f, -0.015f, -0.09f);
    /// <summary>SetParentするライン</summary>
    const float setParentRange = .15f;
    /// <summary>次に遷移させるイベント</summary>
    const ObservableGameEventTrigger.GameEvent nextEvent = ObservableGameEventTrigger.GameEvent.DisplayTitle;

    private void Start()
    {
        Tutorial.GetComponent<Text>();
        this.OnGameEventAsObservable()
            .Where(gEvent => gEvent == ObservableGameEventTrigger.GameEvent.GetTheWeapon)
            .Subscribe(v =>
            {
                FadeInText();
                MovetoHand();
            })
            .AddTo(this);
    }

    /// <summary>右リングの到達座標</summary>
    Vector3 arrivalCoordinatesOfTheRightRing;
    /// <summary>左リングの到達座標</summary>
    Vector3 arrivalCoordinatesOfTheLeftRing;
    public void MovetoHand()
    {
        var update = this.UpdateAsObservable().Publish().RefCount();

        /// <see cref="handDetectionRange"/>迄リングに手を近づけるとリングが手に寄ってくる
        var disposable = update
            .Where(_ =>
            (r_bracelet.position - RightHand.position).magnitude <= handDetectionRange
            || (l_bracelet.position - LeftHand.position).magnitude <= handDetectionRange)
            .Subscribe(_ =>
            {
                r_bracelet.position = Vector3.MoveTowards(r_bracelet.position, targetRightBracelet.position, Time.deltaTime * moveSpeed);
                r_bracelet.rotation = Quaternion.RotateTowards(r_bracelet.rotation, targetRightBracelet.rotation, Time.deltaTime * moveSpeed);
                l_bracelet.position = Vector3.MoveTowards(l_bracelet.position, targetLeftBracelet.position, Time.deltaTime * moveSpeed);
                r_bracelet.rotation = Quaternion.RotateTowards(r_bracelet.rotation, targetLeftBracelet.rotation, Time.deltaTime * moveSpeed);

                arrivalCoordinatesOfTheRightRing = RightHand.position + rightRingPositionOffset;
                arrivalCoordinatesOfTheLeftRing = LeftHand.position + leftRingPositionOffset;
            })
            .AddTo(this);

        // 指定範囲内にリングがきたら親子関係を付けて次のイベントを呼ぶ
        update
            .First(_ =>
            (r_bracelet.position - RightHand.position).magnitude <= setParentRange
            && (l_bracelet.position - LeftHand.position).magnitude <= setParentRange)
            .Subscribe(_ =>
            {
                r_bracelet.position = targetRightBracelet.position;
                l_bracelet.position = targetLeftBracelet.position;
                r_bracelet.rotation = targetRightBracelet.rotation;
                l_bracelet.rotation = targetLeftBracelet.rotation;

                r_bracelet.SetParent(RightHand);
                l_bracelet.SetParent(LeftHand);
                isGetRing = true;
                FadeOutText();
                disposable.Dispose();
            })
            .AddTo(this);
    }

    IDisposable fadeInTextDisposable;
    private bool isGetRing;
    public void FadeInText()
    {
        Tutorial.text = "リングをとれ\nGet the Rings.";
        textAplha = Tutorial.color.a;
        fadeInTextDisposable = Observable.Timer(TimeSpan.FromSeconds(delayTimeOfTextDisplayAnimation))
            .Subscribe(_ =>
            {
                this.UpdateAsObservable()
                .TakeWhile(l => textAplha < 1f && !isGetRing)
                .Subscribe(l =>
                {
                    Tutorial.color = new Color(0, 0, 0, textAplha);
                    textAplha += Time.deltaTime * fadingSpeed;
                });
            })
            .AddTo(this);
    }

    void FadeOutText()
    {
        fadeInTextDisposable.Dispose();

        var disposable_ = new SingleAssignmentDisposable();
        disposable_.Disposable = Observable.EveryFixedUpdate()
                .TakeWhile(l => textAplha > 0f)
                .Subscribe(_ =>
                {
                    Tutorial.color = new Color(0, 0, 0, textAplha);
                    textAplha -= Time.deltaTime * fadingSpeed;
                    if (textAplha <= 0)
                    {
                        Tutorial.color = new Color(0, 0, 0, textAplha);
                        ObservableGameEventTrigger.SetGameEvent(nextEvent);
                        disposable_.Dispose();
                    }
                }, _ => Debug.Log("wwwwwwwwwwwwwwwww"))
                .AddTo(this);
    }
}

