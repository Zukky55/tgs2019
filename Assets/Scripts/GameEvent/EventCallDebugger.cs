﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

public class EventCallDebugger : MonoBehaviour
{
#if UNITY_EDITOR
    private void Start()
    {
        Debug.Log("StageイベントのときにNキーでクリアイベントを呼ぶ");
        this.UpdateAsObservable()
            .Where(_ => Input.GetKeyDown(KeyCode.N))
            .Subscribe(_ => CallEvent(ObservableGameEventTrigger.CurrentEvent))
            .AddTo(this);
    }
#endif

    /// <summary>
    /// 指定したイベントを呼ぶ
    /// </summary>
    /// <param name="currentEvent">現在のイベント</param>
    public void CallEvent(ObservableGameEventTrigger.GameEvent currentEvent)
    {
        switch(currentEvent)
        {
            case ObservableGameEventTrigger.GameEvent.Stage:
                ObservableGameEventTrigger.SetGameEvent(ObservableGameEventTrigger.GameEvent.StageClearEvent);
                break;
        }
    }
}
