﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Linq;
using UniRx;
using UniRx.Triggers;
using UnityEngine.UI;

public class TutorialEvent : MonoBehaviour
{
    [SerializeField] GameObject TutorialRedMagicBullet;
    [SerializeField] GameObject TutorialBlueMagicBullet;
    [SerializeField] Transform redMagicSpawnNode;
    [SerializeField] Transform blueMagicSpawnNode;
    [SerializeField] public Door blueTargetDoor;
    [SerializeField] public Door redTargetDoor;
    [SerializeField] Text tutorialTextBox;
    /// <summary>玉が破壊されてから次の玉が出現する迄のインターバル</summary>
    [SerializeField] float spawnInterval = 1f;
    [SerializeField] float delayTimeOfTextDisplayAnimation = 2f;
    [SerializeField] float textFadingSpeed = 1f;

    /// <summary>赤い玉の参照</summary>
    GameObject redMBRef;
    /// <summary>青い玉の参照</summary>
    GameObject blueMBRef;
    IDisposable redMBDisposable;
    IDisposable blueMBDisposable;
    private void Start()
    {
        this.OnGameEventAsObservable()
            .Where(gEvent => gEvent == ObservableGameEventTrigger.GameEvent.TutorialEvent)
            .Subscribe(v =>
            {
                FadeInText();
                redMBDisposable = redMBRef
                .OnDestroyAsObservable()
                .Subscribe(_ =>
                {
                    Observable.Timer(TimeSpan.FromSeconds(spawnInterval))
                    .Subscribe(l =>
                    {
                        redMBRef = Instantiate(TutorialRedMagicBullet, redMagicSpawnNode.position, Quaternion.identity);
                        var tmb = redMBRef.GetComponent<TutorialMagicBullet>();
                        tmb.SetTargetDoor(redTargetDoor.TargetNode);
                    })
                    .AddTo(this);
                })
                .AddTo(this);

                blueMBDisposable = blueMBRef
                    .OnDestroyAsObservable()
                    .Subscribe(_ =>
                    {
                        Observable.Timer(TimeSpan.FromSeconds(spawnInterval))
                        .Subscribe(l =>
                        {
                            blueMBRef = Instantiate(TutorialBlueMagicBullet, blueMagicSpawnNode.position, Quaternion.identity);
                            var tmb = blueMBRef.GetComponent<TutorialMagicBullet>();
                            tmb.SetTargetDoor(blueTargetDoor.TargetNode);
                        })
                        .AddTo(this);
                    })
                    .AddTo(this);

                // 扉全てチャージ完了したら扉を開くイベントに遷移する
                this.UpdateAsObservable()
                .First(_ => blueTargetDoor.IsCharged && redTargetDoor.IsCharged)
                .Subscribe(_ =>
                {
                    FadeOutText();
                    blueMBDisposable.Dispose();
                    redMBDisposable.Dispose();
                })
                .AddTo(this);
            })
    .AddTo(this);
    }


    float textAplha = 0;
    public void FadeInText()
    {
        tutorialTextBox.text = "同じ色の手で殴れ\nPunch at the same colors\n as your hand";
        textAplha = tutorialTextBox.color.a;
        Observable.Timer(TimeSpan.FromSeconds(delayTimeOfTextDisplayAnimation))
            .Subscribe(_ =>
            {
                this.UpdateAsObservable()
                .TakeWhile(l => textAplha < 1f)
                .Subscribe(l =>
                {
                    tutorialTextBox.color = new Color(0, 0, 0, textAplha);
                    textAplha += Time.deltaTime * textFadingSpeed;
                });
            })
            .AddTo(this);
    }

    void FadeOutText()
    {
        var disposable_ = new SingleAssignmentDisposable();
        disposable_.Disposable = Observable.EveryFixedUpdate()
          .Subscribe(_ =>
          {
              tutorialTextBox.color = new Color(0, 0, 0, textAplha);
              textAplha -= Time.deltaTime * textFadingSpeed;
              if (tutorialTextBox.color.a <= 0)
              {
                  ObservableGameEventTrigger.SetGameEvent(ObservableGameEventTrigger.GameEvent.OpenStoneDoor);
                  disposable_.Dispose();
              }
          })
          .AddTo(this);
    }
}
