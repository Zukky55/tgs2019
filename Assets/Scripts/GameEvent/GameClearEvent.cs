﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Linq;
using UniRx;
using UniRx.Triggers;

/// <summary>
/// ゲームが終了したときに呼ばれるイベント
/// </summary>
public class GameClearEvent : MonoBehaviour
{
    [SerializeField] List<GameObject> spectrums;
    private void Start()
    {
        this.OnGameEventAsObservable()
            .Where(gEvent => gEvent == ObservableGameEventTrigger.GameEvent.StageClearEvent)
            .Subscribe(v =>
            {
                foreach(var spectrum in spectrums)
                {
                    spectrum.SetActive(false);
                }
                ScoreManager.Instance.NonActiveScoreCanvas();
                SoundManager.Instance.StopBGM();
                MusicScoreLoader.Instance.InitializeArray();
                GameManager.Instance.ClearEnemyList();
                foreach(var magicBullet in Conductor.Instance.MagicBullets)
                {
                    Destroy(magicBullet.gameObject);
                }
                Conductor.Instance.Enemies.Clear();   
                ObservableGameEventTrigger.SetGameEvent(ObservableGameEventTrigger.GameEvent.Result);
            })
            .AddTo(this);
    }
}
