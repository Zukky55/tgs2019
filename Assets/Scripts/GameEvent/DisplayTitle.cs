﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Linq;
using UniRx;
using UniRx.Triggers;
using UnityEngine.UI;
public class DisplayTitle : MonoBehaviour
{
    [SerializeField] SpriteRenderer Logo;
    [SerializeField] float fadingSpeed;
    [SerializeField] Text Tutorial;
    [SerializeField] float startEventDelayTime = 1f;
    [SerializeField] float fadeOutDelayTime = 2f;
    AudioSource audio;
    float alpha = 0;
    float textAplha = 0;
    bool AlphaFlag = true;
    bool textAlphaFlag = true;

    private void Awake()
    {
        Tutorial.color = new Color(255, 255, 255, 0);
        audio = Logo.GetComponent<AudioSource>();
    }
    void Start()
    {
        this.OnGameEventAsObservable()
            .Where(gEvent => gEvent == ObservableGameEventTrigger.GameEvent.DisplayTitle)
            .Delay(TimeSpan.FromSeconds(startEventDelayTime))
            .Subscribe(v =>
            {
                alpha = Logo.color.a;
                audio.Play();
                FadeInTitle();

                this.UpdateAsObservable()
                .First(_ => !audio.isPlaying)
                .Subscribe(_ =>
                {
                    ObservableGameEventTrigger.SetGameEvent(ObservableGameEventTrigger.GameEvent.TutorialEvent);
                })
                .AddTo(this);
            })
           .AddTo(this);
    }

    private void FadeInTitle()
    {
        var update = this.UpdateAsObservable()
        .TakeWhile(l => alpha < 1f)
        .Subscribe(l =>
        {
            Logo.color = new Color(255, 255, 255, alpha);
            alpha += Time.deltaTime * fadingSpeed;

            if (alpha >= 1)
            {
                alpha = 1;
                FadeOutTitle();
            }
        })
    .AddTo(this);
    }

    void FadeOutTitle()
    {
        this.UpdateAsObservable()
             .Delay(TimeSpan.FromSeconds(fadeOutDelayTime))
            .Subscribe(v =>
            {
                Logo.color = new Color(255, 255, 255, alpha);
                alpha -= Time.deltaTime * fadingSpeed;

                if (alpha < 0)
                    alpha = 0f;
            })
            .AddTo(this);
    }
}