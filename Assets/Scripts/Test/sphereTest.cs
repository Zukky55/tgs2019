﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
public class sphereTest : MonoBehaviour
{
    Texture2D screenTexture;
    Color DamegeColor;
    Subject<Collider> onTriggerEnterSubject = new Subject<Collider>();
    public IObservable<Collider> OnTriggerEnterAsOSV => onTriggerEnterSubject;
    private void Awake()
    {
        screenTexture = new Texture2D(1, 1, TextureFormat.ARGB32, false);
        DamegeColor = new Color(255, 0, 0, 0.3f);
        screenTexture.SetPixel(0, 0, DamegeColor);
        screenTexture.Apply();
    }
    private void OnTriggerEnter(Collider other)
    {
        onTriggerEnterSubject.OnNext(other);
    }
    MagicBulletSpecifier mbs = MagicBulletSpecifier.BlueMagic;
    private void Start()
    {
        this.OnTriggerEnterAsObservable()
            .Where(col => col.tag == ObjTag.MagicHand.ToString())
            .Select(col => col.GetComponent<MagicHand>())
            .Where(hand => hand.Mbs == mbs)
            .Subscribe(hand =>
            {
                Debug.Log($"{hand.gameObject.tag}と衝突したよ");
            })
            .AddTo(this);


        var limit = 5f;
        var time = TimeSpan.Zero;
        onTriggerEnterSubject
            .Where(col => col.tag == ObjTag.MagicHand.ToString())
            .Select(col => col.gameObject)
            .Subscribe(go =>
            {
                //Debug.Log($"{go.tag}と衝突したよ");
                var disposable = new SingleAssignmentDisposable();
                disposable.Disposable = this.UpdateAsObservable()
                .Timestamp()
                .Select(t => t.Timestamp)
                .Buffer(2, 1)
                .Select(t => t.Last() - t.First())
                .Subscribe(t =>
                {
                    Debug.Log($"onUpdate");
                    toggle = true;
                    time += t;
                    Debug.Log(time.TotalSeconds);
                    if (time.TotalSeconds > limit)
                    {
                        toggle = false;
                        disposable.Dispose();
                        time = TimeSpan.Zero;
                        Debug.Log($"onDisposed{time.TotalSeconds}");
                    }
                })
                .AddTo(this);
            })
            .AddTo(this);
    }

    bool toggle;
    public void OnGUI()
    {
        if (toggle)
            GUI.DrawTexture(Camera.main.pixelRect, screenTexture);
    }
}
