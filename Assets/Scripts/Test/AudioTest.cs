﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
public class AudioTest : MonoBehaviour
{
    [SerializeField] TextMesh debugText;
    // Start is called before the first frame update
    void Start()
    {
        var conductor = Conductor.Instance;
        this.UpdateAsObservable()
            .Subscribe(_ =>
            {
                debugText.text = $"SecondPerBeat = {conductor.SecondPerBeat}\n" +
                $"Time.timeSinceLevelLoad = {Time.timeSinceLevelLoad}\n" +
                $"Music.IsJustChangedBeat() = {Music.IsJustChangedBeat()}";
            })
            .AddTo(this);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
