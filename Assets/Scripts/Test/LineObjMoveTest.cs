﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineObjMoveTest : MonoBehaviour
{
    [SerializeField] float moveSpeed;
    [SerializeField] float moveTurnTime;
    private float timeCount;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timeCount += Time.deltaTime;

        if (timeCount <= moveTurnTime)
        {
            transform.Translate(moveSpeed, 0, 0);
        }



        if ( timeCount > moveTurnTime)
        {
            transform.Translate(-moveSpeed, 0, 0);
            if (timeCount > 2 * moveTurnTime)
                timeCount = 0.0f;

        }

    }
}
