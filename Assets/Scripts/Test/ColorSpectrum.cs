﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

public class ColorSpectrum : MonoBehaviour
{
    [SerializeField] AudioSpectrum spectrum;
    [SerializeField] Light light;
    [SerializeField] List<Light> lights;
    [SerializeField] float scale;



    // Use this for initialization
    void Start()
    {
        var c = light.color;
        this.UpdateAsObservable()
            .Subscribe(_ =>
            {
                c.r = spectrum.MeanLevels[0] * scale;
                c.g = spectrum.MeanLevels[1] * scale;
                c.b = spectrum.MeanLevels[2] * scale;
                light.color = c;
                Debug.Log($"spectrum.MeanLevels[0] * scale = {spectrum.MeanLevels[0] * scale}\n" +
                    $"spectrum.MeanLevels[1] * scale = {spectrum.MeanLevels[1] * scale}\n" +
                    $"spectrum.MeanLevels[2] * scale = {spectrum.MeanLevels[2] * scale}");
            })
            .AddTo(this);
    }
}
