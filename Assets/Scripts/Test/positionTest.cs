﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class positionTest : MonoBehaviour
{
    [SerializeField] TextMesh textLog;
    [SerializeField] List<Transform> transforms;
    private void Update()
    {
        textLog.text = "";
        foreach (var t in transforms)
        {
            textLog.text += $"{t.name} = {t.position}\n";
        }
    }
}
