﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
public class GolemTest : MonoBehaviour
{

    Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();

        this.UpdateAsObservable()
            .Where(_ => Input.GetKeyDown(KeyCode.T))
            .Do(_=>Debug.Log($"call"))
            .Subscribe(_ =>
            {
                animator.SetTrigger("Good");
            })
            .AddTo(this);
    }
}
