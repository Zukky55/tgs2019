﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

public class LightSpectrum : MonoBehaviour
{
    [SerializeField] AudioSpectrum spectrum;
    [SerializeField] Light light;
    [SerializeField] List<Light> lights;
    [SerializeField] float scale;



    // Use this for initialization
    void Start()
    {
        this.UpdateAsObservable()
            .Subscribe(_ =>
            {
                for (int band = 0; band < lights.Count; band++)
                {
                    var l = lights[band];
                    var intensity = l.intensity;
                    intensity = spectrum.MeanLevels[band] * scale;
                    l.intensity = intensity;
                    Debug.Log($"spectrum.MeanLevels[band] = {spectrum.MeanLevels[band]*scale}");
                    // test
                }
                if (light != null)
                    light.intensity = spectrum.MeanLevels[0];
            })
            .AddTo(this);
    }
}
