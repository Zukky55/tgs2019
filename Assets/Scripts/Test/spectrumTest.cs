﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

public class spectrumTest : MonoBehaviour
{
    [SerializeField] AudioSpectrum spectrum;
    [SerializeField] List<Transform> cubes;
    [SerializeField] float scale;
    [SerializeField] float colorScale;

    private void Start()
    {
        this.UpdateAsObservable()
            .Subscribe(_ =>
            {
                for (int i = 0; i < cubes.Count; i++)
                {
                    var cube = cubes[i];
                    var localScale = cube.localScale;
                    localScale.y = spectrum.MeanLevels[i] * scale;
                    cube.localScale = localScale;

                    var renderer = cube.gameObject.GetComponent<Renderer>();
                    var mat =renderer.material;
                    var color = mat.color;
                    color.r = spectrum.MeanLevels[0] * colorScale;
                    color.g = spectrum.MeanLevels[1] * colorScale;
                    color.b = spectrum.MeanLevels[2] * colorScale;
                    mat.color = color;
                }
            })
            .AddTo(this);
    }

}
