﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UniRx.Triggers;


public class TimeTest : MonoBehaviour
{
    [SerializeField, Range(0, 1)] float scale = 1;
    [SerializeField] AudioSource audio;
    // Start is called before the first frame update
    void Start()
    {
        this.UpdateAsObservable()
            .Subscribe(_ =>
            {
                Time.timeScale = scale;
                if (audio.isPlaying)
                    audio.pitch = scale;
            })
            .AddTo(this);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
