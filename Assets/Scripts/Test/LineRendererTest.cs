﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class LineRendererTest : MonoBehaviour
{
    [SerializeField] Transform start;
    [SerializeField] Transform end;
    [SerializeField] LineRenderer lineRenderer;

    [SerializeField] float width;

    private void Start()
    {
        lineRenderer.alignment = LineAlignment.View;
    }
    // Update is called once per frame
    void Update()
    {
        var positions = new Vector3[2] { start.position, end.position };

        lineRenderer.SetPositions(positions);
        lineRenderer.startWidth = width;
        lineRenderer.endWidth= width;
    }
}
