﻿using UnityEngine;
using System.Collections;

namespace KaitenZushi.PlayScene
{
    public class EffectManager : SingletonMonoBehaviour<EffectManager>
    {

        public ParticleSystem[] particles;

        public void PlayEffect(int num)
        {
            particles[num].Play(true);
        }
    }
}