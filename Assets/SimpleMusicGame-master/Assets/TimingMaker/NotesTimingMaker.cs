﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NotesTimingMaker : MonoBehaviour
{

    [SerializeField] AudioSource _audioSource;
    private float startTime = 0;
    [SerializeField] CSVWriter _CSVWriter;

    private bool isPlaying = false;
    public GameObject startButton;
    int magicType = 0;

    //private void Start()
    //{
    //    startButton.GetComponent<Button>().onClick.AddListener(() => StartMusic());
    //}

    void Update()
    {
        if(isPlaying)
        {
            DetectKeys();
        }
    }

    public void StartMusic()
    {
        startButton.SetActive(false);
        _audioSource.Play();
        startTime = Time.timeSinceLevelLoad;
        isPlaying = true;
    }

    void DetectKeys()
    {
        if(Input.GetKeyDown(KeyCode.LeftArrow))
        {
            magicType = 0;
            Debug.Log("左手で弾ける魔法");
        }
        else if(Input.GetKeyDown(KeyCode.RightArrow))
        {
            magicType = 1;
            Debug.Log("右手で弾ける魔法");
        }
        else if(Input.GetKeyDown(KeyCode.DownArrow))
        {
            magicType = 2;
            Debug.Log("弾けない魔法");
        }
        else if(Input.GetKeyDown(KeyCode.UpArrow))
        {
            magicType = 3;
            Debug.Log("両手で弾ける魔法");
        }

        if(Input.GetKeyDown(KeyCode.F))
            WriteNotesInfo(0,magicType);

        if(Input.GetKeyDown(KeyCode.D))
            WriteNotesInfo(1,magicType);

        if(Input.GetKeyDown(KeyCode.S))
            WriteNotesInfo(2,magicType);

        if(Input.GetKeyDown(KeyCode.J))
            WriteNotesInfo(3,magicType);

        if(Input.GetKeyDown(KeyCode.K))
            WriteNotesInfo(4,magicType);

        if(Input.GetKeyDown(KeyCode.L))
            WriteNotesInfo(5,magicType);
    }

    void WriteNotesInfo(int node,int magicType)
    {
        string zahyou = "";
        switch(node)
        {
            case 0:
                zahyou = "左ダンゴロ";
                break;
            case 1:
                zahyou = "左ショケケ";
                break;
            case 2:
                zahyou = "左ゴーレム";
                break;
            case 3:
                zahyou = "右ダンゴロ";
                break;
            case 4:
                zahyou = "右ショケケ";
                break;
            case 5:
                zahyou = "右ゴーレム";
                break;
        }

        string magic = "";
        switch(magicType)
        {
            case 0:
                magic = "赤い魔法";
                break;
            case 1:
                magic = "青い魔法";
                break;
            case 2:
                magic = "弾けない魔法";
                break;
            case 3:
                magic = "両手で弾く魔法";
                break;
        }
        _CSVWriter.WriteCSV(GetTiming().ToString() + "," + node.ToString() + "," + magicType.ToString());
        Debug.Log($"{GetTiming()},{zahyou},{magic}");
    }

    float GetTiming()
    {
        return Time.timeSinceLevelLoad - startTime;
    }
}