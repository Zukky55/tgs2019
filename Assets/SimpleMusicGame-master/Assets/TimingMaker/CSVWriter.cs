﻿using UnityEngine;
using System.Collections;
using System.IO;

public class CSVWriter : MonoBehaviour
{
    string fixedPath;
    string fileName; // 保存するファイル名
    [SerializeField,Header("書き込むCSVファイル")] TextAsset musicScore;
    // CSVに書き込む処理
    public void WriteCSV(string txt)
    {
        StreamWriter streamWriter;
        FileInfo fileInfo;
        fixedPath = "SimpleMusicGame-master/";
        fileName = musicScore.name;
        fileInfo = new FileInfo(Application.dataPath + "/" + fixedPath + fileName + ".csv");
        streamWriter = fileInfo.AppendText();
        streamWriter.WriteLine(txt);
        streamWriter.Flush();
        streamWriter.Close();
    }
}
